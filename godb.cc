//
// JavaScript Compiler
//
// � George Bouris
//

#include "godb.h"

using namespace cgo::db;

cgo::db::Database::Database()
{
	Rowcounter = 0;
}

cgo::db::Database::~Database()
{
	Rowcounter = 0;
	Result.clear();
	Fields.clear();
	DB->Close();
	DB = NULL;
}

v8::Handle<v8::Value> cgo::db::DBConstruct(const v8::Arguments& args)
{	
	if (!args.IsConstructCall()) 
		return v8::ThrowException(v8::String::New("Invalid call format"));
	
	v8::String::Utf8Value dbname(args[0]);
	v8::Local<v8::Object> self = args.Holder();

	Database *dbo = new Database();
	dbo->DB = new SQLiteDatabase();

	if (!dbo->DB->Open(*dbname))
	{
		std::string errorstr = "Cannot open database: ";
		errorstr.append(*dbname);
		return v8::ThrowException(v8::String::New(errorstr.c_str()));
	}

	self->SetInternalField(0, v8::External::New(dbo));
	
	return self;
}

v8::Handle<v8::Value> cgo::db::DBClose(const v8::Arguments& args)
{
	Database *tDBobj = static_cast<Database*>((v8::Local<v8::External>::Cast(args.Holder()->GetInternalField(0)))->Value());

	tDBobj->~Database();
	
	return v8::Boolean::New(true);
}

v8::Handle<v8::Value> cgo::db::DBExecute(const v8::Arguments& args)
{	
	v8::String::Utf8Value sql(args[0]);
	v8::Local<v8::Object> self = args.Holder();
	
	Database *tDBobj  =  static_cast<Database*>(v8::Local<v8::External>::Cast(self->GetInternalField(0))->Value());
	
	if (!tDBobj->DB)
		return v8::ThrowException(v8::String::New(ERR_CLOSED_DATABASE));
	
	tDBobj->Result.clear();
	tDBobj->Fields.clear();
	tDBobj->Rowcounter = 0;

#if ENABLE_LIVE_DEBUG
	if (Settings.DEBUG_TIME_SPLIT)
			T_DB_begin = clock();
#endif

	std::string error;
	tDBobj->Result = tDBobj->DB->Query(*sql, tDBobj->Fields, error);

#if ENABLE_LIVE_DEBUG
	if (Settings.DEBUG_TIME_SPLIT)
		T_DB += utils::Diffclock(clock(), T_DB_begin);
#endif

	if (error.length() > 0)
		return v8::ThrowException(v8::String::New(error.c_str()));
	
	return self;
}

v8::Handle<v8::Value> cgo::db::DBEof(const v8::Arguments& args)
{	
	Database *tDBobj = static_cast<Database*>((v8::Local<v8::External>::Cast(args.Holder()->GetInternalField(0)))->Value());
	
	if (!tDBobj->DB)
		return v8::ThrowException(v8::String::New(ERR_CLOSED_DATABASE));

	if ((int)tDBobj->Result.size() <= (int)tDBobj->Rowcounter)
		return v8::Boolean::New(true);
	
	return v8::Boolean::New(false);
}

v8::Handle<v8::Value> cgo::db::DBTotalRecords(const v8::Arguments& args)
{	
	Database *tDBobj = static_cast<Database*>((v8::Local<v8::External>::Cast(args.Holder()->GetInternalField(0)))->Value());
	
	if (!tDBobj->DB)
		return v8::ThrowException(v8::String::New(ERR_CLOSED_DATABASE));

	return v8::Integer::New(tDBobj->Result.size());
}

v8::Handle<v8::Value> cgo::db::DBGet(const v8::Arguments& args)
{	
	v8::String::Utf8Value item(args[0]);
    
	Database *tDBobj = static_cast<Database*>((v8::Local<v8::External>::Cast(args.Holder()->GetInternalField(0)))->Value());
	
	if (!tDBobj->DB)
		return v8::ThrowException(v8::String::New(ERR_CLOSED_DATABASE));
		
	std::vector<std::string> *row = &tDBobj->Result.at(tDBobj->Rowcounter);
	
	int at;
	if (utils::IsNumeric(*item))
		at = atoi(*item);
	else
	{
		if (tDBobj->Fields.find(*item) == tDBobj->Fields.end())
		{
			std::string err = "No such field: ";
			err.append(*item);
			return v8::ThrowException(v8::String::New(err.c_str()));
		}
		at = tDBobj->Fields[*item];		
	}
	
	int sz1 = row->size() - 1;
	if (sz1 < at)
		return v8::Undefined();
	
	return v8::String::New(row->at(at).c_str());	
}

v8::Handle<v8::Value> cgo::db::DBNextRow(const v8::Arguments& args)
{	
	Database *tDBobj = static_cast<Database*>((v8::Local<v8::External>::Cast(args.Holder()->GetInternalField(0)))->Value());
	
	if (!tDBobj->DB)
		return v8::ThrowException(v8::String::New(ERR_CLOSED_DATABASE));

	tDBobj->Rowcounter++;

	return v8::Boolean::New(true);
}

