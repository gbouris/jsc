<!DOCTYPE html>
<@ 
include("go.utils.keygenerator");
include("go.encryption.aes"); 
@>
<html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
		</head>
			
				  
<body>
<div style="width:640px">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<!--#GOBEGI_N lines:2;name:FieldSet--><fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
		  </fieldset><!--#GOEND-->
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
		
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>

<BR><BR>
 
</body>
</html>


