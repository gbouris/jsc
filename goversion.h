//
// JavaScript Compiler
//
// � George Bouris
//

#define _FILEVERSION 1,0,2,1841
#define _PRODUCTVERSION 2,0,0
#define _FILEVERSION_STR "1, 0, 2, 1841"
#define _PRODUCTVERSION_STR "2, 0, 0"
#define _FileDescription "JavaScript Compiler"
#define _InternalName "JavaScript Compiler"
#define _LegalCopyright "(C) George Bouris"
#define _OriginalFilename "jsc"
#define _ProductName "JavaScript Compiler"
