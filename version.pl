$file = "goversion.h";

open FILE, $file or die $!;
while (<FILE>) { 
	$content.=$_; 
}

$content =~ m/_FILEVERSION (.*?),(.*?),(.*?),(.*?)\n/;
$version1 = $1;
$version2 = $2;
$version3 = $3;
$version4 = $4;
$version4_current = $4;
$version4++;

if ($version4>=65536){
	$version4 = 0;
	$version3++;
}

if ($version3>=65536){
	$version3 = 0;
	$version2++;
}

if ($version2>=65536){
	print "\n*** VERSION NUMBER NEEDS RESET ***\n\n";
	exit(1);
}

$content =~ s/_FILEVERSION (.*?),(.*?),(.*?),(.*?)\n/_FILEVERSION $version1,$version2,$version3,$version4\n/;
$content =~ s/_FILEVERSION_STR "(.*?), (.*?), (.*?), (.*?)"\n/_FILEVERSION_STR "$version1, $version2, $version3, $version4"\n/;
print "FILEVERSION $version1,$version2,$version3,$version4_current";

open FILE, ">$file" or die $!;
print FILE $content; 
close FILE;	
