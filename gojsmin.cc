//
// JavaScript Compiler
//
// � George Bouris
//
/* jsmin.c MODIFIED
   2012-11-18

Copyright (c) 2002 Douglas Crockford  (www.crockford.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "gojsmin.h"

#if ENABLE_JS_MINIFY
#include "goexceptions.h"

using namespace cgo::minifier;

static int   theA;
static int   theB;
static int   theLookahead;
static int   theX;
static int   theY;
const char* csource;
std::string rsource;

void cgo::minifier::Minify(std::string& source)
{
	csource = source.c_str();

	theA = NULL;
	theB = NULL;
	theLookahead = '\0';
	theX = '\0';
	theY = '\0';

	Jsmin();
	
	if (rsource[0] == '\n')
		source = rsource.erase(0,1);
	else
		source = rsource;
}

static void cgo::minifier::Error(char* s)
{
	throw GOMinException(s);
}

static int cgo::minifier::IsAlphanum(int c)
{
    return ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') ||
        (c >= 'A' && c <= 'Z') || c == '_' || c == '$' || c == '\\' ||
        c > 126);
}

static int cgo::minifier::Get()
{
	int c = theLookahead;
    theLookahead = '\0';
    if (c == '\0') {
        c = *csource++;
    }
    if (c >= ' ' || c == '\n' || c == '\0') {
        return c;
    }
    if (c == '\r') {
        return '\n';
    }
    return ' ';
}

static int cgo::minifier::Peek()
{
	theLookahead = Get();
    return theLookahead;
}

static int cgo::minifier::Next()
{
	int c = Get();
    if  (c == '/') {
        switch (Peek()) {
        case '/':
            for (;;) {
				c = Get();
                if (c <= '\n') {
                    break;
                }
            }
            break;
        case '*':
            Get();
            for (;;) {
				switch (Get()) {
                case '*':
                    if (Peek() == '/') {
                        Get();
                        c = ' ';
                    }
                    break;
                case '\0':
                    Error("Unterminated comment.");
					return '\0';
                }
            }
            break;
        }
    }
    theY = theX;
    theX = c;
    return c;
}

static void cgo::minifier::Action(int d)
{
	switch (d) {
    case 1:
		rsource += theA;
	    if (
            (theY == '\n' || theY == ' ') &&
            (theA == '+' || theA == '-' || theA == '*' || theA == '/') &&
            (theB == '+' || theB == '-' || theB == '*' || theB == '/')
        ) {
			rsource += theY;
        }
    case 2:
        theA = theB;
        if (theA == '\'' || theA == '"' || theA == '`') {
            for (;;) {
				rsource += theA;
                theA = Get();
                if (theA == theB) {
                    break;
                }
                if (theA == '\\') {
					rsource += theA;
                    theA = Get();
                }
                if (theA == '\0') {
                    Error("Unterminated string literal.");
					return;
                }
            }
        }
    case 3:
        theB = Next();
        if (theB == '/' && (
            theA == '(' || theA == ',' || theA == '=' || theA == ':' ||
            theA == '[' || theA == '!' || theA == '&' || theA == '|' ||
            theA == '?' || theA == '+' || theA == '-' || theA == '~' ||
            theA == '*' || theA == '/' || theA == '\n'
        )) {
            
			rsource += theA;
            if (theA == '/' || theA == '*') {
                rsource += ' ';
            }
            rsource += theB;
            for (;;) {
				theA = Get();
                if (theA == '[') {
                    for (;;) {
                        rsource += theA;
                        theA = Get();
                        if (theA == ']') {
                            break;
                        }
                        if (theA == '\\') {
                            
							rsource += theA;
                            theA = Get();
                        }
                        if (theA == '\0') {
                            Error("Unterminated set in Regular Expression literal.");
							return;
                        }
                    }
                } else if (theA == '/') {
                    switch (Peek()) {
                    case '/':
                    case '*':
                        Error("Unterminated set in Regular Expression literal.");
						return;
                    }
                    break;
                } else if (theA =='\\') {
                    
					rsource += theA;
                    theA = Get();
                }
                if (theA == '\0') {
                    Error("Unterminated Regular Expression literal.");
					return;
                }
				rsource += theA;
            }
            theB = Next();
        }
    }
}

static void cgo::minifier::Jsmin()
{
	if (Peek() == 0xEF) {
        Get();
        Get();
        Get();
    }
    theA = '\n';
    Action(3);
    while(theA != '\0') {
		switch (theA) {
        case ' ':
            Action(IsAlphanum(theB) ? 1 : 2);
            break;
        case '\n':
            switch (theB) {
            case '{':
            case '[':
            case '(':
            case '+':
            case '-':
            case '!':
            case '~':
                Action(1);
                break;
            case ' ':
                Action(3);
                break;
            default:
                Action(IsAlphanum(theB) ? 1 : 2);
            }
            break;
        default:
            switch (theB) {
            case ' ':
                Action(IsAlphanum(theA) ? 1 : 3);
                break;
            case '\n':
                switch (theA) {
                //case '}':
                case ']':
                //case ')':
                case '+':
                case '-':
                case '"':
                case '\'':
                case '`':
                    Action(1);
                    break;
                default:
                    Action(IsAlphanum(theA) ? 1 : 3);
                }
                break;
            default:
                Action(1);
                break;
            }
        }
    }
}
#endif
