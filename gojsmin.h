//
// JavaScript Compiler
//
// � George Bouris
//

#include "godef.h"

#if ENABLE_JS_MINIFY
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <boost/regex.hpp>

namespace cgo
{	
	namespace minifier
	{	
		void Minify(std::string& source);
		static void Error(char* s);
		static int IsAlphanum(int c);
		static int Get();
		static int Peek();
		static int Next();
		static void Action(int d);
		static void Jsmin();
	}
}
#endif
