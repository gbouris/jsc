//
// JavaScript Compiler
//
// � George Bouris
//

#include "gosqlite.h"

using namespace cgo::db;

cgo::db::SQLiteDatabase::SQLiteDatabase()
{
	database = NULL;
}

cgo::db::SQLiteDatabase::~SQLiteDatabase()
{
	
}

bool cgo::db::SQLiteDatabase::Open(const char* filename)
{
	if (sqlite3_open_v2(filename, &database, SQLITE_OPEN_READWRITE, NULL) == SQLITE_OK)
		return true;
		
	return false;
}

std::vector< std::vector<std::string> > cgo::db::SQLiteDatabase::Query(const char* query, 
																	  boost::unordered_map<std::string, int>& fields, 
																	  std::string& error)
{
	sqlite3_stmt* statement;
	std::vector< std::vector<std::string> > results;
	
	if (sqlite3_prepare_v2(database, query, -1, &statement, 0) == SQLITE_OK)
	{
		int cols = sqlite3_column_count(statement);
		int result = 0;
		
		for(int ncol = 0; ncol < cols; ncol++)
			fields[sqlite3_column_name(statement, ncol)]=ncol;
		
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			std::vector<std::string> values;
			values.reserve(cols);
			for(int ncol = 0; ncol < cols; ncol++)
			{
				char* cptr = (char*)sqlite3_column_text(statement, ncol);
				if (cptr)
					values.push_back(cptr);
				else
					values.push_back("");
			}
			results.push_back(values);
		}   
		sqlite3_finalize(statement);
	}
	
	std::string errorm = sqlite3_errmsg(database);
	if (errorm != "not an error")
	{
		error = errorm;
	}
	
	return results;  
}

void cgo::db::SQLiteDatabase::Close()
{
	sqlite3_close(database);   
}


