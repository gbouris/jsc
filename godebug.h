//
// JavaScript Compiler
//
// � George Bouris
//

#include <v8.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp> 
#include <boost/algorithm/string/regex.hpp>
#include <boost/algorithm/string/detail/trim.hpp>
#include "godef.h"
#include "goutils.h"

namespace cgo
{
	namespace debug
	{ 	
		std::string ReportException(const v8::TryCatch* handler);
		inline void FixDebugComments(std::string& psource);
		inline std::string FixDebugComments2(std::string& psource);
		std::string PrepareLines(std::string& psource);
		std::string PrepareLinesIncludes(std::string& psource, const std::string& line);
		std::string HandleError(const std::string& error, const std::string& jsexacterrpart);
		std::string ErrorToXML(const std::string& error);
		inline std::string GetSourceCodeLine(const int srcline, std::string& errpart, const std::string& jsexacterrpart, int& errcol, int &errlength);
		inline std::vector<std::string> CleanJSError(std::string& jserrormsg);
#if ENABLE_LIVE_DEBUG
		void WriteDebugFile(const std::string content);		
#endif
	}

	extern GOJSCSettings Settings;
	extern const char* JS_SRC;
	extern bool ACTIVE_DEBUG;
}
