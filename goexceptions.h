//
// JavaScript Compiler
//
// � George Bouris
//

#include <iostream>
#include "goutils.h"

namespace cgo
{
	extern GOJSCSettings Settings;

	class GOJSException : public std::runtime_error 
	{
	public:
		GOJSException(const std::string& msg) : std::runtime_error(utils::StringToChar(utils::FormatError(msg))){};
		~GOJSException(){};
	};

	class GOLocalException : public std::runtime_error 
	{
	public:
		GOLocalException(const std::string& msg) : std::runtime_error(utils::StringToChar(utils::FormatError(msg))){};
		~GOLocalException(){};
	};

#if ENABLE_JS_MINIFY
	class GOMinException : public std::runtime_error 
	{
	public:
		GOMinException(const char* msg) : std::runtime_error(msg){};
		~GOMinException(){};
	};
#endif
}
