//
// JavaScript Compiler
//
// © George Bouris
//

#include "gojsc.h"
#include "goexceptions.h"

using namespace cgo;

int main(int argc, char* argv[]) 
{

#if ENABLE_LIVE_DEBUG
	if (Settings.DEBUG_TIME)
		T_begin = clock();
#endif

	char* file = GetParameteres(argc, argv);

	if (Settings.IS_WEB)
		printf("HTTP/1.0 200 OK\nContent-type: text/html\n\n");
		
	if (!ExecuteScriptFile(file))
		return 0;
	
#if ENABLE_LIVE_DEBUG
	if (Settings.DEBUG_TIME)
	{
		if (Settings.DEBUG_TIME_SPLIT)
		{
			double dif = utils::Diffclock(clock(), T_begin);
			if (T_PRINT_begin > 0)
				dif -= utils::Diffclock(clock(), T_PRINT_begin);

			std::cout << std::endl << "\n Execution time (ms)" << std::endl
					  << " __________________" << std::endl << std::endl
					  << " GOJSC :\t" << dif - T_V8 << std::endl
					  << " JSEng :\t" <<  T_V8 - T_DB  << std::endl 
					  << " SQLite:\t" << T_DB << std::endl  
					  << " __________________" << std::endl << std::endl 
					  << " Total:\t\t" << dif  << " ms" << std::endl;
		}
		else
			std::cout << std::endl << "Execution time: " << utils::Diffclock(clock(), T_begin) << " ms"  << std::endl;
	}
#endif

 return 0;
}

inline char* cgo::GetParameteres(int argc, char* argv[])
{
	if (argc < 2)
	{
		printf("No Parameters - Usage: %s  <filename>\n", argv[0]);
		exit(0);
	}
	
	char *file;

	for(int i = 1; i < argc; i++)
	{  
		 if (strcmp(argv[i], "-w") == 0){ Settings.IS_WEB = true; Settings.RETURN_SOURCE = false; } 
		 else if (strcmp(argv[i], "-e") == 0) Settings.RETURN_SOURCE = false;  
		 else if (strcmp(argv[i], "-eo") == 0) Settings.EXECUTE_ONLY = true;  
		 else if (strcmp(argv[i], "-s") == 0) Settings.PRESERVE_SPACES = true; 
		 else if (strcmp(argv[i], "-d") == 0) Settings.CHECK_DUPLICATE_IDS = true; 
		 else if (strcmp(argv[i], "-lp") == 0){ if (argc>i+1) Settings.LIB_PATH = (std::string)argv[i+1]; ++i; }
		 else if (strcmp(argv[i], "-bp") == 0){ if (argc>i+1) Settings.BASE_PATH = (std::string)argv[i+1]; ++i; }
		 else if (strcmp(argv[i], "-ulp") == 0){ if (argc>i+1) Settings.USR_LIB_PATH = (std::string)argv[i+1]; ++i; }
		 else if (strcmp(argv[i], "-xmle") == 0) Settings.RETURN_XML_ERROR = true; 
		 else if (strcmp(argv[i], "-st") == 0){ Settings.RETURN_SYNTAX_TREE = true; Settings.RETURN_SOURCE = false; }
		 else if (strcmp(argv[i], "-v") == 0) ShowVersion();
#if ENABLE_LIVE_DEBUG
		 else if (strcmp(argv[i], "-f") == 0) Settings.DEBUG_TO_FILE = true;
		 else if (strcmp(argv[i], "-dt") == 0) Settings.DEBUG_TIME = true; 
		 else if (strcmp(argv[i], "-dtx") == 0){ Settings.DEBUG_TIME = true; Settings.DEBUG_TIME_SPLIT = true; } 
		 else if (strcmp(argv[i], "-dtp") == 0){ Settings.DEBUG_TIME_WITH_PRINT = true; Settings.DEBUG_TIME = true; }
		 else if (strcmp(argv[i], "-dtpx") == 0){ Settings.DEBUG_TIME_WITH_PRINT = true; Settings.DEBUG_TIME = true; Settings.DEBUG_TIME_SPLIT = true; } 
#endif		
#if ENABLE_CACHE	
		 else if (strcmp(argv[i], "-cp") == 0){ if (argc>i+1) Settings.CACHE_PATH = (std::string)argv[i+1]; ++i; }
		 else if (strcmp(argv[i], "-ec") == 0) Settings.ENABLE_ACTIVE_CACHE = true;
		 else if (strcmp(argv[i], "-rc") == 0 || Settings.RETURN_SYNTAX_TREE) Settings.RESET_CACHE = true;
#endif
		 else if ((char)argv[i][0] != '-') file = argv[i];
	 }
	
	if (!file)
	{
		printf("No <filename> parameter\n", argv[0]);
		exit(0);
	}

	return file;
}

extern "C"
{
	__declspec(dllexport) const char* cgo::Compile(
													const char* scriptsource, 
													const char* original_source,
													int& ref_haserrors, 
													const char* base_path, 
													const char* usr_lib_path,
													const bool return_xml_errror, 
													const bool return_source,
													const bool return_syntax_tree, 
													const bool preserve_spaces, 
													const bool check_duplicate_ids,
													const bool executy_only
#if ENABLE_CACHE
													,const bool enable_active_cache
#endif
													)
														
  { 
		  Settings.USR_LIB_PATH = usr_lib_path;
		  Settings.RETURN_XML_ERROR = return_xml_errror;
		  Settings.RETURN_SOURCE = return_source;
		  Settings.RETURN_SYNTAX_TREE = return_syntax_tree;
		  Settings.PRESERVE_SPACES = preserve_spaces;
		  Settings.CHECK_DUPLICATE_IDS = check_duplicate_ids;
		  Settings.EXECUTE_ONLY = executy_only;
#if ENABLE_CACHE
		  Settings.ENABLE_ACTIVE_CACHE = enable_active_cache;
#endif

		  if (strlen(base_path) > 0) 
		  {
				  Settings.LIB_PATH.insert(0, base_path);
#if ENABLE_CACHE
				  Settings.CACHE_PATH.insert(0, base_path);
#endif
		  } 

		  if (Settings.RETURN_SYNTAX_TREE) Settings.RETURN_SOURCE = false;

		  std::string content(scriptsource); 
		  std::string scriptresult;

		  if (strlen(original_source) > 0)
			JS_SRC = original_source;
		  else
			JS_SRC = scriptsource;
	try {

#if ENABLE_CACHE
		if (!Settings.EXECUTE_ONLY)
		{
			if (Settings.ENABLE_ACTIVE_CACHE)
			{
				int fcrc=utils::GetCrc32(content);
				char lbuffer[33];
				itoa(fcrc,lbuffer,10);
				std::string cname(Settings.CACHE_PATH);
				cname.append(lbuffer);
				cname.append(".cache");
			
				bool cacheExists  = false;
				if (!Settings.RESET_CACHE)
					cacheExists = HandleCacheRead(cname, content);
		
				if (!cacheExists || Settings.RESET_CACHE)
				{
					parser::Parse(content);
				
					if (!ACTIVE_DEBUG)
						boost::thread(&HandleCacheWrite, cname, content);
				}
			}
			else
				parser::Parse(content);
		}
#else
		  if (!Settings.EXECUTE_ONLY)
			  parser::Parse(content);
#endif	

#if ENABLE_LIVE_DEBUG
		  if (Settings.DEBUG_TIME_SPLIT)
			T_V8_begin = clock();
#endif
		  bool res = true;
	  
		  { 
			v8::HandleScope handle_scope;
			v8::Persistent<v8::Context> context;

			InitV8(context);

#if ENABLE_LIVE_DEBUG			  
			if (Settings.DEBUG_TO_FILE)
				boost::thread(&debug::WriteDebugFile, content);
#endif		
			res = ExecuteString(v8::String::New(content.c_str()), scriptresult) ;

			if (Settings.CHECK_DUPLICATE_IDS) 
				CheckDuplicateIDs(scriptresult);

			context->Exit();
			context.Dispose();
		}	
		v8::V8::Dispose();
	}
	catch(const GOLocalException& goem)
	{	
#if ENABLE_LIVE_DEBUG		  
		if (Settings.DEBUG_TIME_SPLIT)
			T_V8 += utils::Diffclock(clock(), T_V8_begin);		
#endif
		ref_haserrors = 1;
		if (Settings.RETURN_XML_ERROR)
			return utils::StringToChar(debug::ErrorToXML(goem.what()));
		else
			return goem.what();
	}
	catch(const GOJSException& goem)
	{	
		if (Settings.EXECUTE_ONLY)
		{
#if ENABLE_LIVE_DEBUG		  
			if (Settings.DEBUG_TIME_SPLIT)
				T_V8 += utils::Diffclock(clock(), T_V8_begin);		
#endif
			ref_haserrors = 1;
			if (Settings.RETURN_XML_ERROR)
				return utils::StringToChar(debug::ErrorToXML(goem.what()));
			else
				return goem.what();
		}
		
		try{
			ACTIVE_DEBUG = true; 
			v8::HandleScope handle_scope;
			content = std::string(scriptsource); 
			content = debug::PrepareLines(content);
			ResetCounters();
			parser::Parse(content);
			scriptresult = "";
			ExecuteString(v8::String::New(content.c_str()), scriptresult);
		}
		catch(const GOJSException& goem)
		{
#if ENABLE_LIVE_DEBUG		  
			if (Settings.DEBUG_TIME_SPLIT)
				T_V8 += utils::Diffclock(clock(), T_V8_begin);		
#endif
			ACTIVE_DEBUG = false;
			ref_haserrors = 1;
			if (Settings.RETURN_XML_ERROR)
				return utils::StringToChar(debug::ErrorToXML(goem.what()));
			else
				return goem.what();
		}
	}

#if ENABLE_LIVE_DEBUG		  
		if (Settings.DEBUG_TIME_SPLIT)
			T_V8 += utils::Diffclock(clock(), T_V8_begin);
#endif

		if (Settings.RETURN_SOURCE && !ACTIVE_DEBUG && !Settings.EXECUTE_ONLY)
		{
			std::string tfinal = "print(";
			tfinal.append(JS_BUFFERFUNC);
			tfinal.append("());");
			
			boost::erase_first(content, tfinal);

			return utils::StringToChar(content.c_str()); 
		}
		else
			return utils::StringToChar(scriptresult.c_str());  
  }
}

inline void cgo::InitV8(v8::Persistent<v8::Context>& context)
{
	v8::Handle<v8::FunctionTemplate> cgo_templ = v8::FunctionTemplate::New();
	cgo_templ->SetClassName(v8::String::New(JS_GO_NAMESPACE));
	cgo_templ->Set("getParameter", v8::FunctionTemplate::New(GetParameter));
	if (Settings.IS_WEB)
		cgo_templ->Set("getQueryString", v8::FunctionTemplate::New(GetParameter));
	
	v8::Handle<v8::ObjectTemplate> cgo_proto = cgo_templ->PrototypeTemplate();

	v8::Local<v8::FunctionTemplate> cgodb_templ = cgo_templ->v8::FunctionTemplate::New(db::DBConstruct);
	cgodb_templ->SetClassName(v8::String::New("db"));
		
	v8::Local<v8::Template> proto_t_cgodbexec = cgodb_templ->PrototypeTemplate();
	proto_t_cgodbexec->Set("execute", v8::FunctionTemplate::New(db::DBExecute));
	proto_t_cgodbexec->Set("eof", v8::FunctionTemplate::New(db::DBEof));
	proto_t_cgodbexec->Set("nextrow", v8::FunctionTemplate::New(db::DBNextRow));
	proto_t_cgodbexec->Set("close", v8::FunctionTemplate::New(db::DBClose));
	proto_t_cgodbexec->Set("get", v8::FunctionTemplate::New(db::DBGet));
	proto_t_cgodbexec->Set("totalrecords", v8::FunctionTemplate::New(db::DBTotalRecords));

	v8::Handle<v8::ObjectTemplate> global = v8::ObjectTemplate::New();
	global->Set(v8::String::New("print"), v8::FunctionTemplate::New(Print));
	
	v8::Handle<v8::ObjectTemplate> cgodb_inst = cgodb_templ->InstanceTemplate();
	cgodb_inst->SetInternalFieldCount(1);

	cgo_templ->Set(v8::String::New("db"), cgodb_templ);
	global->Set(v8::String::New(JS_GO_NAMESPACE), cgo_templ);

	context = v8::Context::New(NULL, global);

	if (context.IsEmpty()) 
		printf("Error creating context\n");

	context->Enter();
}

v8::Handle<v8::Value> cgo::Print(const v8::Arguments &args) 
{
  for(int i = 0; i < args.Length(); i++) 
  {
    v8::HandleScope handle_scope;
    v8::String::Utf8Value str(args[i]);
    return v8::String::New(*str);
  }

  return v8::Undefined();
}

v8::Handle<v8::Value> cgo::GetParameter(const v8::Arguments &args) 
{
	std::string qitem(*v8::String::Utf8Value(args[0]));

	if (Settings.IS_WEB)
	{
		std::vector<std::string> strs;
		boost::split(strs, std::string(getenv("QUERY_STRING")), boost::is_any_of("&"));
	
		int sz = strs.size();
		int pos;

		for(int i = 0; i < sz; i++)
		{
			pos = utils::Index(strs[i].c_str() , "=");

			if (boost::iequals(strs[i].substr(0, pos), qitem))
				return v8::String::New(strs[i].erase(0, pos + 1).c_str());
		}
	}
	
  return v8::String::New("");
}

void cgo::ShowVersion()
{
	v8::HandleScope handle_scope;

	std::string pv = STR(_PRODUCTVERSION);
	boost::replace_all(pv, ",", ".");
	std::string fv = STR(_FILEVERSION);
	boost::replace_all(fv, ",", ".");

	std::cout << _ProductName << " " << pv << " - " << _LegalCopyright << std::endl << std::endl
				 << _FileDescription << " " << fv << std::endl << std::endl
				 << "JavaScript Engine: " << Version() << std::endl << std::endl << std::endl
				 << "Configuration: " << std::endl
				 << "Cache=" << ((ENABLE_CACHE)?"Enabled":"Disabled")  
				 << " MinJS=" << ((ENABLE_JS_MINIFY)?"Enabled":"Disabled") 
				 << " L.D=" << ((ENABLE_LIVE_DEBUG)?"Enabled":"Disabled")<< std::endl; 

	v8::V8::Dispose();
	exit(0); 
}

std::string cgo::Version() 
{
  return std::string(v8::V8::GetVersion());
}

bool cgo::ExecuteString(const v8::Handle<v8::String> source, std::string& scriptresult) 
{
  v8::HandleScope handle_scope;
  v8::TryCatch try_catch;
  
  v8::Handle<v8::Script> script = v8::Script::Compile(source);
  
  if (script.IsEmpty()) 
  { 
	throw GOJSException(debug::ReportException(&try_catch));
	return false;
  } 
  else 
  {
    v8::Handle<v8::Value> result = script->Run();
	
    if (result.IsEmpty()) 
	{
		assert(try_catch.HasCaught());	  
		throw GOJSException(debug::ReportException(&try_catch));
		return false;
    } 
	else 
	{
		assert(!try_catch.HasCaught());
		if (!result->IsUndefined()) 
		{
			v8::String::Utf8Value str(result);
			scriptresult.append(*str);
		}
	return true;
   }
 }

  return false;
}
 
bool cgo::ExecuteScriptFile(const char* name)
{
  FILE* file = fopen(name, "rb");
  if (!file) 
  {	
	std::string error_msg = "File not found: ";
	error_msg.append(name);
	std::cout << error_msg;
	return false;
  }
  
  fseek(file, 0, SEEK_END);
  int size = ftell(file);
  rewind(file);

  char* chars = new char[size + 1];
  chars[size] = '\0';

  for(int i = 0; i < size;) 
  {
    int read = static_cast<int>(fread(&chars[i], 1, size - i, file));
    i += read;
  }
  fclose(file);
  
  std::string content = std::string(chars);

  delete[] chars;
  int dummy = 0;

  content = Compile(content.c_str(),
					"",
					dummy,
					Settings.BASE_PATH.c_str(),
					Settings.USR_LIB_PATH.c_str(),
					Settings.RETURN_XML_ERROR,
					Settings.RETURN_SOURCE,
					Settings.RETURN_SYNTAX_TREE,
					Settings.PRESERVE_SPACES,
					Settings.CHECK_DUPLICATE_IDS,
					Settings.EXECUTE_ONLY
#if ENABLE_CACHE
					,Settings.ENABLE_ACTIVE_CACHE
#endif
					);

#if ENABLE_LIVE_DEBUG
	if (!Settings.DEBUG_TIME || Settings.DEBUG_TIME_WITH_PRINT)
	{
		if (Settings.DEBUG_TIME_SPLIT)
			T_PRINT_begin=clock();
	 
		printf("%s",content.c_str());
	}
#else
  	printf("%s",content.c_str());
#endif	 

  return true;
}

#if ENABLE_CACHE
inline void cgo::HandleCacheWrite(std::string& cname, const std::string& content)
{
	FILE* file = NULL;
	file = fopen(cname.c_str(),"wb");
	if(file!=NULL)
	{ 
		fwrite(content.c_str(), content.length(), 1, file);
		fclose(file);
	}
	else
		std::cerr << "Cache folder \"" << Settings.CACHE_PATH << "\" is missing" << std::endl;
}

inline bool cgo::HandleCacheRead(std::string& cname, std::string& content)
{
	FILE* file = NULL;

	file = fopen(cname.c_str(),"rb");
	if(file != NULL)
	{
		fseek(file, 0, SEEK_END);
		int size = ftell(file);
		rewind(file);

		char* chars = new char[size + 1];
		chars[size] = '\0';

		for(int i = 0; i < size;) 
		{
			int read = static_cast<int>(fread(&chars[i], 1, size - i, file));
			i += read;
		}
		fclose(file);

		content = std::string(chars);
			
		return true;
	}
	
	return false;
}
#endif

inline void cgo::ResetCounters()
{
	UNQ_IDS.clear();
	UNQ_INCLUDES.clear();
}

inline void cgo::CheckDuplicateIDs(std::string& scriptresult)
{
	std::string errstr;

	if (parser::CheckDuplicateIDs(scriptresult, errstr))
	{
		std::string error_msg = "Duplicate ID found: ";
		error_msg.append(errstr);
				
		throw GOLocalException(error_msg);
	}
}


