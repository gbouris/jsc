<html>
<head>
<title>GO</title>
<style>
body, table, td, tr{
	font-family: Helvetica, Verdana;
	font-size: 12px;
}

table td{
	border:1px solid #f1f1f1;
	padding:2px;
}
</style>
</head>

<body>

<@ 

var a = [34, 203, 3, 746, 200, 984, 198, 764, 9];
 
function bubbleSort(a)
{
    var swapped;
    do {
        swapped = false;
        for (var i=0; i < a.length-1; i++) {
            if (a[i] > a[i+1]) {
                var temp = a[i];
                a[i] = a[i+1];
                a[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}
 



function ColorLuminance(hex, lum) {  
    hex = String(hex).replace(/[^0-9a-f]/gi, '');  
    if (hex.length < 6) {  
        hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];  
    }  
    lum = lum || 0;  
    var rgb = "#", c, i;  
    for (i = 0; i < 3; i++) {  
        c = parseInt(hex.substr(i*2,2), 16);  
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);  
        rgb += ("00"+c).substr(c.length);  
    }  
    return rgb;  
}

var i;
@>
<table>
<tr><th>unsorted</th><th>bubble sorted</th></tr>
<tr><td>
<@

for (i=0;i<a.length;i++){
	write(a[i]+"<BR>");
}

@>
</td><td>
<@
bubbleSort(a);
for (i=0;i<a.length;i++){
	write(a[i]+"<BR>");
}

@></td></tr>
</table>


<table width="100%">
<tr><td valign="top" width="50%">
<table style="width:100%;border:1px solid #193652;border-collapse:collapse">
<@
var color="#336699";
var bgcolort="#CAD7E4";
var ht=100;
for (i=1;i<=ht;i++){
@>
<tr style="color:<@=color;@>;background:<@=bgcolort;@>" id="tr<@=i;@>"><td style="width:33%">[<@=color;@>] - [<@=bgcolort;@>]</td><td style="width:33%">To i einai <@=i;@></td><td style="width:33%">leei o <@="lalakis"+((ht+1)-i);@></td></tr>
<@
color=ColorLuminance(color, 0.015);
bgcolort=ColorLuminance(bgcolort, -0.015);
}@>
</table>
</td>
<td valign="top" width="50%" ><table id="tb2" style="width:100%;border-collapse:collapse;font-size:12px"></table></td></tr>
</table>

<@
/*
	var oconn = new ActiveXObject("ADODB.Connection") ;
	oconn.Provider="Microsoft.Jet.OLEDB.4.0";
	oconn.Open("Data Source=c:/Northwind.mdb");
	var rs = new ActiveXObject("ADODB.Recordset");
	rs.Open("SELECT * FROM Asiakkaat", oconn);
	rs.MoveFirst;
	while(!rs.eof)
	{
		 document.write(rs.fields("Asiakastunnus")+" - "+rs.fields("Yritys")+" - "+rs.fields(2)+" - "+rs.fields(3)+"<BR>\n");
	   rs.movenext;
	}
	
	rs.close;
	oconn.close;
*/
@>
<script>
<@
for (i=1;i<=ht;i++){
@>
setTimeout(function(){
	document.getElementById("tr<@=i;@>").style.display="none";
	document.getElementById("tb2").innerHTML=document.getElementById("tr<@=i;@>").innerHTML+document.getElementById("tb2").innerHTML;
	},(<@=i;@>*20));	
<@}@>
rev();
function rev(){
if (document.getElementById("tr<@=ht;@>").style.display!='none'){ 
setTimeout(rev,20);
	return;
	}
<@
for (var i2=1;i2<=ht;i2++){
	@>
		setTimeout(function(){
		document.getElementById("tr<@=i2;@>").style.display="";
		},(<@=i2;@>*20));	
<@}
@>
}
</script>


</body>
</html>