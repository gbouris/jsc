//<GOJSIMPORT>
//	
//	<GOJSCONFIG>
//		title = "go.utils.keygenerator";
//		version = "1.2";
//		aurhor = "George Bouris";
//		description = "Keygenerator is a blah blah...";
//		documentation = "blah blah...";
//	</GOJSCONFIG>
//
//  <GOJSEXPORT>

					function makekey(digits,splinum)
					{
					    var text = "";
					    var possible = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
					        
					    for( i=0; i < digits; i++ )
					        text += possible.charAt(Math.floor(Math.random() * possible.length));
							if (splinum){
								  digits+=(digits/splinum)-1;
							    for ( i=splinum; i < digits; i+=splinum+1)
							    		text=splice(text,i,0,"-");
							}
					    return text;
					}
					
					function splice(str, idx, rem, s ) {
					    return (str.slice(0,idx) + s + str.slice(idx + Math.abs(rem)));
					}
					
// </GOJSEXPORT>
//			
//</GOJSIMPORT>