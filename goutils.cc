//
// JavaScript Compiler
//
// � George Bouris
//
#include "goutils.h"

using namespace cgo::utils;

char* cgo::utils::StringToChar(const std::string& str)
{
	int sz = str.length();
	char* retPtr(new char[sz + 1]);
	std::copy(str.begin(), str.end(), retPtr);
	retPtr[sz] = '\0';

	return retPtr;
}

bool cgo::utils::IsNumeric(const char* str)
{
  while(*str)
  {
    if (!isdigit(*str))
      return false;

    str++;
  }
  return true;
}

int cgo::utils::Index(const char* str1, const char* str2)
{
  int t;
  const char* p, *p2;

  for(t=0; str1[t]; t++) 
  {
    p = &str1[t];
    p2 = str2;

    while(*p2 && *p2==*p) 
	{
      p++;
      p2++;
    }

    if(!*p2) 
		return t; 
  }
   return -1; 
}

std::string cgo::utils::GetMatch(const std::string& base, const std::string& match)
{
	boost::regex re(match);
	boost::smatch matches;
	
	if (boost::regex_search(base, matches, re))
		return std::string(matches[1]);
	
	return "";
}

std::string cgo::utils::XMLEncode(std::string& error)
{
    std::ostringstream sret;

    for( std::string::const_iterator iter = error.begin(); iter != error.end(); iter++ )
    {
         unsigned char c = (unsigned char)*iter;
		 
         switch( c )
         {
             case '&': sret << "&amp;"; break;
             case '<': sret << "&lt;"; break;
             case '>': sret << "&gt;"; break;
             case '"': sret << "&quot;"; break;
             case '\'': sret << "&apos;"; break;

             default:
              if (c < 32 || c > 127)
                  sret << "&#" << (unsigned int)c << ";";
              else
                  sret << c;
         }
    }

	return sret.str();
}

void  cgo::utils::MinifySimple(std::string& source)
{
	boost::regex pattern("\\)\\s*\\n+\\s*\\{", boost::regex_constants::perl);
	source = boost::regex_replace(source, pattern, ") {");

	pattern.assign("\\n+", boost::regex_constants::perl);
	source = boost::regex_replace(source, pattern, " ");

	pattern.assign("\\s+", boost::regex_constants::perl);
	source = boost::regex_replace(source, pattern, " ");
}

std::string cgo::utils::FormatError(const std::string& errstr)
{ 
	std::string error_msg(errstr);

	if (Settings.IS_WEB)	
	{
		error_msg.insert(0, "<span style=\"color:#222;font-family:Courier\">");
		error_msg.append("</span><BR>");
	}

	return error_msg;
}

#if ENABLE_CACHE
int cgo::utils::GetCrc32(const std::string& my_string) 
{
    boost::crc_32_type result;
    result.process_bytes(my_string.data(), my_string.length());

    return result.checksum();
}
#endif

#if ENABLE_LIVE_DEBUG
double cgo::utils::Diffclock(const clock_t& clock1, const clock_t& clock2)
{
	double diffticks = clock1 - clock2;
	double diffms = (diffticks*1000) / CLOCKS_PER_SEC;
	
	return diffms;
}
#endif


