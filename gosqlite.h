//
// JavaScript Compiler
//
// � George Bouris
//

#include <string>
#include <vector>
#include <boost/unordered_map.hpp>
#include "sqlite3.h"
#include "goutils.h"

namespace cgo
{
	namespace db
	{
		class SQLiteDatabase
		{
		public:
			SQLiteDatabase();
			~SQLiteDatabase();
			bool Open(const char* filename);
			void Close();
			std::vector< std::vector<std::string> > Query(const char* query, 
														 boost::unordered_map<std::string, int>& fields, 
														 std::string& error);
		private:
			sqlite3* database;
	
		};
	}
}



