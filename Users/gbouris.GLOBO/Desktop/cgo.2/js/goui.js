﻿/*
 * UI JavaScript Library v1.0
 *
 * © Copyright George Bouris
 */

var goui = new GOUI();

function GOUI() {
    this.settings = {
        //uselocalgoext: true
    };

    this.setVal = function (objid, val) {
        $("#" + objid).val(val);
    }
    
    this.textWidth = function (text) {
        var mdiv = document.createElement("span");
        mdiv.innerHTML = text;
        mdiv.style.display = 'none';
        document.body.appendChild(mdiv);
        var width = $(mdiv).outerWidth();
        document.body.removeChild(mdiv);
        return width;
    };
    
    this.paginate = function (obj, pdiv, rpp) {
        var totalrecords = $('#' + obj + ' tr').length - 2;
        $('#' + pdiv).smartpaginator({
            totalrecords: totalrecords,
            recordsperpage: rpp,
            datacontainer: obj,
            dataelement: 'tr'
        });
        $("#" + obj).tablesorter({
            paginatortrec: totalrecords,
            paginatordiv: pdiv,
            paginatortable: obj,
            paginatorpages: rpp,
            sortInitialOrder: "desc"
        });
    }
    
    this.filter = function (obj, pdiv, rpp, iobj) {
        var val = iobj.value;
        if (val.trim().length > 0) {
            $.uiTableFilter($('#' + obj), val);
            $('#' + pdiv).hide();
            $(iobj).next('.icon_clear').stop().fadeTo(700, 0.6);
            $(iobj).next('.icon_clear').bind('click', function () {
                $(iobj).val('');
                goui.filter(obj, pdiv, rpp, iobj);
            });
        } else {
            $('#' + pdiv).show();
            var usersTable = $("#" + obj);
            usersTable.trigger("update").trigger("sorton", [usersTable.get(0).config.sortList]).trigger("appendCache").trigger("applyWidgets");
            $(iobj).next('.icon_clear').stop().fadeTo(700, 0);
        }
    }
    
    
    var pageEventIFrame = null;

    function sendToTop(str) {
        //str=str.replace(undefined,"");
        //var astr=str.split(";");
        //str='';
        //for (var i=0;i<astr.length;i++){
        //	str+=astr[i]+"<BR>";
        //}
        //document.getElementById("cev").innerHTML=str;
        return;
        if (pageEventIFrame == null) {
            pageEventIFrame = document.createElement("IFRAME");
            pageEventIFrame.setAttribute("src", str);
            pageEventIFrame.style.display = 'none';
            document.documentElement.appendChild(pageEventIFrame);
        } else {
            pageEventIFrame.setAttribute("src", str);
            //alert($(pageEventIFrame).attr('src'));
        }
        //iframe.parentNode.removeChild(iframe);
        //iframe = null;
    }

    function transferEvent(e) {
        var nname;
        if (($(e.target).attr('cchecked'))) {
            nname = "CHECKBOX"
        } else if (($(e.target).attr('cchecked'))) {
            nname = "CHECKBOX"
        } else {
            nname = e.target.nodeName;
        }
        //if (!e.target.id || nname=='DIV'){return;}
        var data = 'target:' + nname + ';id:' + e.target.id + ';type:' + e.type + ';which:' + e.which + ';x:' + e.pageX + ';y:' + e.pageY + ';selected:' + (jQuery(e.target).is(":selected")) + ';checked:' + ($(e.target).attr('cchecked'));
        sendToTop('/cgo/event/' + data);
        //document.location.href='/cgo/event/'+data;
        //document.getElementById("cev").innerHTML=data;
    }
    
    this.sliderValue = function (obj) {
        var trg = '#' + $(obj).attr('id') + 'value';
        var prefix = $(trg).attr('prefix');
        var suffix = $(trg).attr('suffix');
        var ftext = '';
        if (prefix) {
            ftext += prefix + " ";
        }
        ftext += $(obj).val();
        if (suffix) {
            ftext += " " + suffix;
        }
        $(trg).html(ftext);
    }
    
    //$(document).bind("submit click textinput select load touchstart touchend touchcancel", transferEvent); 
    //mousemove blur, change, focus, reset, resize, scroll, select, submit, zoom, keydown, keyup touchmove
    //click, dblclick, mousedown, mousemove, mouseup, mouseout, mouseover, mousewheel, dragstart, dragend
    //touchstart, touchend, touchmove, touchcancel
    //click textinput resize scroll select submit zoom load touchstart touchend touchcancel
    
    this.checkForm = function (form1, extra) {
        var stype = '';
        var slength = '';
        var svalue = '';
        var sname = '';
        var nme = '';
        var ctype = '';
        var smand = '2';
        var disctype = '';
        var salphnum = '';
        var snospaces = '';
        var sdischar = '';
        var sisemail = '';
        for (var i = 0; i < form1.length; i++) {
            stype = form1[i].type;
            sname = form1[i].name;
            svalue = form1[i].value;
            ctype = form1[i].getAttribute('ctype');
            slength = form1[i].getAttribute('length');
            smand = form1[i].getAttribute('mand');
            salphnum = form1[i].getAttribute('alphnum');
            snospaces = form1[i].getAttribute('nospaces');
            sdischar = '';
            sdischar = form1[i].getAttribute('dischar');
            sisemail = '';
            sisemail = form1[i].getAttribute('isemail');
            nme = form1[i].getAttribute('nme');
            
            if (nme == '') {
                nme = '-1';
            }
            
            if (ctype == 'num') {
                disctype = 'Number'
            } else if (ctype == 'date') {
                disctype = 'Date'
            } else {
                disctype = 'Text'
            }
            
            if (!stype) {
                continue;
            }
            
            if ((form1[i].value.length > 0) && (ctype == 'num') && (!isNumeric(svalue))) {
                valert('You cannot enter Text into this field', true, form1[i]);
                //form1[i].focus();
                if (extra == 1) {
                    var tid = eval('document.getElementById("' + nme + '")');
                    if (tid) {
                        tid.className = 'fterror';
                    }
                }
                return false;
            } else if ((form1[i].value.length > 0) && (ctype == 'file')) {
                var acc = form1[i].getAttribute('acc');
                var accarr = acc.split(",");
                var fvalid = false;
                for (var accnt = 0; accnt < accarr.length; accnt++) {
                    if (checkFileExt(svalue.toLowerCase(), accarr[accnt].toLowerCase())) {
                        fvalid = true;
                        break;
                    }
                }
                if (!fvalid) {
                    valert('Incorrect file format!', true);
                    form1[i].value = "";
                    //form1[i].focus();
                    if (extra == 1) {
                        var tid = eval('document.getElementById("' + nme + '")');
                        if (tid) {
                            tid.className = 'fterror';
                        }
                    }
                    return false;
                }
            } else if ((form1[i].value.length > 0) && (ctype == 'date') && (!this.isValidDate(svalue))) {
                this.valert('The Date is not valid!', true, form1[i]);
                //form1[i].focus();
                if (extra == 1) {
                    var tid = eval('document.getElementById("' + nme + '")');
                    if (tid) {
                        tid.className = 'fterror';
                    }
                }
                return false;
            } else if ((stype.toLowerCase() == 'radio') || (stype.toLowerCase() == 'checkbox')) {
                if ((smand == '1') && (this.isVisible(form1[i]))) {
                    var tsR = eval('form1.' + form1[i].name);
		    						var chkF = false;
                    var tsRlength = tsR.length;
                    if (!tsRlength) {
                        if (tsR.checked) {
                            chkF = true;
                        }
                    } else {
                        for (var ch = 0; ch < tsR.length; ch++) {
                            if (tsR[ch].checked) {
                                chkF = true;
                                break;
                            }
                        }
                    }
                    if (!chkF) {
                        this.valert('Please, click a choice!', true, form1[i]);
                        //form1[i].focus();
                        if (extra == 1) {
                            var tid = eval('document.getElementById("' + nme + '")');
                            if (tid) {
                                tid.className = 'fterror';
                            }
                        }
                        return false;
                    }
                }
            } else if ((stype.toLowerCase() == 'select-one') || (stype.toLowerCase() == 'select-multiple')) {
                if ((smand == '1') && (this.isVisible(form1[i]))) {
                    var chkF = false;
                    if (form1[i].selectedIndex > -1) {
                        chkF = true;
                    }
                    if (!chkF) {
                        this.valert('Please, select a choice!', true);
                        //form1[i].focus();
                        if (extra == 1) {
                            var tid = eval('document.getElementById("' + nme + '")');
                            if (tid) {
                                tid.className = 'fterror';
                            }
                        }
                        return false;
                    }
                }
            } else if ((stype.toLowerCase() == 'text') || (stype.toLowerCase() == 'password') || (stype.toLowerCase() == 'textarea')) {
                if ((smand == '1') && (this.isVisible(form1[i]))) {
                    if (this.Trim(svalue).length < 1) {
                        this.valert('Please, enter ' + disctype + ' into the selected field', true, form1[i]);
                        //form1[i].focus();
                        if (extra == 1) {
                            var tid = eval('document.getElementById("' + nme + '")');
                            if (tid) {
                                tid.className = 'fterror';
                            }
                        }
                        return false;
                    }
                }
                
                if (salphnum == '1') {
                    if ((this.isAlphaNumeric(form1[i].value)) && (ctype == 'text') && (form1[i].value.length > 0)) {
                        if (document.all) {
                            var snans = vconfirm("Please, use only alphanumeric characters!\n\nDo you want to remove the non AlphaNumeric characters automatically?");
                            if (snans) {
                                form1[i].value = form1[i].value.replace(/[#$%^&*()-+\\'"´¨,%@!|\]\[:;=]/g, "");
                            }
                            form1[i].focus();
                        } else {
                            var tval = form1[i].value.replace(/[#$%^&*()-+\\'"´¨,%@!|\]\[:;=]/g, "");
                            var snans = vconfirm("Please, use only alphanumeric characters!<BR><BR>Do you want to remove the non AlphaNumeric characters automatically?", 'document.' + form1.name + '.' + sname + '.value=\\\'' + tval + '\\\';document.' + form1.name + '.' + sname + '.focus();document.' + form1.name + '.' + sname + '.select();');
                        }
                        if (extra == 1) {
                            var tid = eval('document.getElementById("' + nme + '")');
                            if (tid) {
                                tid.className = 'fterror';
                            }
                        }
                        return false;
                    }
                }
                
                if (snospaces == '1') {
                    if (form1[i].value.match(/\s/g)) {
                        var snans = confirm("Please, do not use [spaces] into this field!\n\nDo you want to remove the [spaces] automatically?");
                        if (snans) {
                            form1[i].value = form1[i].value.replace(/\s*/g, "");
                        }
                        form1[i].focus();
                        if (extra == 1) {
                            var tid = eval('document.getElementById("' + nme + '")');
                            if (tid) {
                                tid.className = 'fterror';
                            }
                        }
                        return false;
                    }
                }
                
                if ((sdischar != '') && (sdischar != null)) {
                    if ((disableChar(form1[i].value, sdischar)) && (ctype == 'text') && (form1[i].value.length > 0)) {
                        var snans = confirm("Character [ " + sdischar + " ] is now allowed into this field!\n\nDo you want to remove the character [ " + sdischar + " ] automatically?");
                        if (snans) {
                            form1[i].value = eval('form1[i].value.replace(/[' + sdischar + ']/g, "");')
                        }
                        form1[i].focus();
                        if (extra == 1) {
                            var tid = eval('document.getElementById("' + nme + '")');
                            if (tid) {
                                tid.className = 'fterror';
                            }
                        }
                        return false;
                    }
                }
                
                if (sisemail == '1') {
                    if (!echeck(form1[i].value)) {
                        this.valert('Incorrect email address', true, form1[i]);
                        //form1[i].focus();
                        return false;
                    }
                }
                
                if (svalue.length > parseInt(slength)) {
                    this.valert('You cannot enter more than ' + slength + ' characters!', true, form1[i]);
                    //form1[i].focus();
                    if (extra == 1) {
                        var tid = eval('document.getElementById("' + nme + '")');
                        if (tid) {
                            tid.className = 'fterror';
                        }
                    }
                    return false;
                }
            }
            var tid = eval('document.getElementById("' + nme + '")');
            if (tid) {
                tid.className = 'ftdright';
            }
        }
        return true;
    }
    this.isVisible = function (fid) {
        if ($(fid).is(':visible')) {
            return true;
        }
        return false;
    }
    
    this.isAlphaNumeric = function (value) {
         var alphaExp = /^[0-9a-zA-Z]+$/;
				if(value.match(alphaExp)){
					return true;
				}else{
					return false;
				}
    }

    function disableChar(pStr, Tchar) {
        var pattern = eval('/[' + Tchar + ']/');
        return pattern.exec(pStr);
    }
    
    this.isNumeric = function (pTest) {
        return $.isNumeric(pTest);
    }
    
    this.isValidDate = function (dateStr) {
        var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
        var datePat2 = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})\s+(\d{1,2})(\:|-)(\d{1,2})(\:|-)(\d{1,2})$/;
        var datePat3 = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})\s+(\d{1,2})(\:|-)(\d{1,2})$/;
        var matchArray = dateStr.match(datePat);
        var matchArray2 = dateStr.match(datePat2);
        var matchArray3 = dateStr.match(datePat3);
        
        if ((matchArray == null) && (matchArray2 == null) && (matchArray3 == null)) {
            return false;
        }
        
        if (matchArray != null) {
            month = matchArray[3];
            day = matchArray[1];
            year = matchArray[4];
        } else if (matchArray2 != null) {
            month = matchArray2[3];
            day = matchArray2[1];
            year = matchArray2[4];
        } else if (matchArray3 != null) {
            month = matchArray3[3];
            day = matchArray3[1];
            year = matchArray3[4];
        }
        
        if (month < 1 || month > 12) {
            return false;
        }
        
        if (day < 1 || day > 31) {
            return false;
        }
        
        if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
            return false;
        }
        
        if (month == 2) {
            var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
            if (day > 29 || (day == 29 && !isleap)) {
                return false;
            }
        }
        
        if (matchArray2 != null) {
            var hour = matchArray2[5];
            var mins = matchArray2[7];
            var secs = matchArray2[9];
            if ((!this.isNumeric(hour)) || (hour < 0) || (hour > 24)) {
                return false;
            } else if ((!this.isNumeric(mins)) || (mins < 0) || (mins > 59)) {
                return false;
            } else if ((!this.isNumeric(secs)) || (secs < 0) || (secs > 59)) {
                return false;
            }
        } else if (matchArray3 != null) {
            var hour = matchArray3[5];
            var mins = matchArray3[7];
            if ((!this.isNumeric(hour)) || (hour < 0) || (hour > 24)) {
                return false;
            } else if ((!this.isNumeric(mins)) || (mins < 0) || (mins > 59)) {
                return false;
            }
        }
        return true;
    }

    function echeck(str) {
        var at = "@"
        var dot = "."
        var lat = str.indexOf(at);
				var lstr = str.length;
        var ldot = str.indexOf(dot);
        
				if (str.indexOf(at) == -1) {
            return false
        }
        
        if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
            return false
        }
        
        if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
            return false
        }
        
        if (str.indexOf(at, (lat + 1)) != -1) {
            return false
        }
        
        if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
            return false
        }
        
        if (str.indexOf(dot, (lat + 2)) == -1) {
            return false
        }
        
        if (str.indexOf(" ") != -1) {
            return false
        }
        
        return true
    }

    function checkFileExt(fln, ext) {
        if (fln.lastIndexOf("." + ext) == -1) {
            return false;
        }
        return true;
    }

    function LTrim(str) {
        var whitespace = new String(" \t\n\r");
        var s = new String(str);
        if (whitespace.indexOf(s.charAt(0)) != -1) {
            var j = 0,
                i = s.length;
            while (j < i && whitespace.indexOf(s.charAt(j)) != -1) j++;
            s = s.substring(j, i);
        }
        return s;
    }

    function RTrim(str) {
        var whitespace = new String(" \t\n\r");
        var s = new String(str);
        if (whitespace.indexOf(s.charAt(s.length - 1)) != -1) {
            var i = s.length - 1;
            while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1) i--;
            s = s.substring(0, i + 1);
        }
        return s;
    }
    this.Trim = function (str) {
        return RTrim(LTrim(str));
    }
    this.valert = function (msg, centered, pfocused) {
        if ($.browser.msie && $.browser.version < 9) {
            alert(msg);
            return;
        }
        if (top.document.getElementById("valrt")) {
            return;
        }
        //  var pfocused=document.activeElement;
        var alrt = top.document.createElement('div');
        alrt.className = 'alert';
        alrt.id = "valrt"
        alrt.style.zIndex = 9999999999;
        //<img src="images/explanation_mark.png">
        alrt.innerHTML = '<BR><div class="alerttext"><b>Information</b><BR><BR>' + msg + '</div><BR><BR><input type="submit" value="OK" class="alertbutton">';
        alrt.addEventListener('touchmove', function () {
            event.preventDefault();
        });

				var alrtbg = document.createElement('div');
        alrtbg.style.position = 'absolute';
        alrtbg.style.top = '0px';
        //alrtbg.style.background='#000'; 
        //alrtbg.style.opacity='0.5'; 
        alrtbg.style.left = '0px';
        alrtbg.style.overflow = 'hidden';
        alrtbg.style.display = 'none';
        alrtbg.id = 'valrtbg';
        alrtbg.addEventListener('touchmove', function () {
            event.preventDefault();
        });
        if (document.body && (document.body.scrollWidth || document.body.scrollHeight)) {
            var pageWidth = document.body.scrollWidth + 'px';
            var pageHeight = document.body.scrollHeight + 'px';
        } else if (document.body.offsetWidth) {
            var pageWidth = document.body.offsetWidth + 'px';
            var pageHeight = document.body.offsetHeight + 'px';
        } else {
            var pageWidth = '100%';
            var pageHeight = '100%';
        }
        alrtbg.style.zIndex = 9999999998;
        alrtbg.style.width = pageWidth;
        alrtbg.style.height = pageHeight;
        alrtbg.style.display = 'block';
        alrt.style.display = 'none';
        top.document.getElementsByTagName("body")[0].appendChild(alrtbg);
        top.document.getElementsByTagName("body")[0].appendChild(alrt);
        //zoomIn(alrt);
        $(alrt).show(); //toggle("fast");
        $(alrt).bind('click', function () {
            var va = top.document.getElementById('valrt');
            if (va) {
                top.document.getElementsByTagName("body")[0].removeChild(va);
                top.document.getElementsByTagName("body")[0].removeChild(top.document.getElementById('valrtbg'));
            } else {
                return true;
            }
            if (pfocused) {
                pfocused.focus();
            }
            return false;
        });
        
        $(top.document).bind('keydown', function (e) {
            var key = (e.keyCode ? e.keyCode : e.which);
            if (key == 13) {
                var va = top.document.getElementById('valrt');
                if (va) {
                    top.document.getElementsByTagName("body")[0].removeChild(va);
                    top.document.getElementsByTagName("body")[0].removeChild(top.document.getElementById('valrtbg'));
                } else {
                    return true;
                }
                if (pfocused) {
                    pfocused.focus();
                }
                return false;
            }
        });
        //	window.scroll(0,findPosY(alrt)-10);
    }

    function findPosX(obj) {
        var curleft = 0;
        if (obj.offsetParent) while (1) {
            curleft += obj.offsetLeft;
            if (!obj.offsetParent) break;
            obj = obj.offsetParent;
        } else if (obj.x) curleft += obj.x;
        return curleft;
    }

    function findPosY(obj) {
        var curtop = 0;
        if (obj.offsetParent) while (1) {
            curtop += obj.offsetTop;
            if (!obj.offsetParent) break;
            obj = obj.offsetParent;
        } else if (obj.y) curtop += obj.y;
        return curtop;
    }

    function GetWidth() {
        var x = 0;
        if (self.innerHeight) {
            x = self.innerWidth;
        } else if (document.documentElement && document.documentElement.clientHeight) {
            x = document.documentElement.clientWidth;
        } else if (document.body) {
            x = document.body.clientWidth;
        }
        return x;
    }

    function GetHeight() {
        var y = 0;
        if (self.innerHeight) {
            y = self.innerHeight;
        } else if (document.documentElement && document.documentElement.clientHeight) {
            y = document.documentElement.clientHeight;
        } else if (document.body) {
            y = document.body.clientHeight;
        }
        return y;
    }

}
/*******************************************************
 *
 *    END GOUI CLASS
 *
 ********************************************************/

 
/*******************************************************
 *
 *    GOUI DOCUMENT READY
 *
 ********************************************************/
$(document).ready(function () {
    
    window.alert = function (arg) {
        goui.valert(arg);
    }
    
    $(document).bind("contextmenu", function (e) {
        return false;
    });
    
    $("input[type=capture]").each(function (index) {
        var tid = $(this).attr('id');
        var bid = "button" + $(this).attr('id');
        $(this).attr('readonly', true);
        $(this).css('width', '65%');
        $(this).after('<button class="capturebutton" onclick="' + $(this).attr("onclick") + '" id="button' + $(this).attr('id') + '" type="button">' + $(this).attr('buttonlabel') + '</button>');
        $("#" + bid).css('height', $(this).outerHeight());
        var dif = ($(this).offset().top - $("#" + bid).offset().top) - 1;
        $("#" + bid).css({
            "top": dif + "px"
        });
        $("#" + bid).bind('click', function () {
            $("#" + bid).addClass("capturebuttonselected");
            $("#" + tid).focus();
        });
        $("#" + tid).bind('blur', function () {
            $("#" + bid).removeClass("capturebuttonselected");
        });
        $("#" + tid).bind('click', function () {
            $("#" + bid).addClass("capturebuttonselected");
        });
    });
    
    $("input[type=localfile]").each(function (index) {
        var tid = $(this).attr('id');
        var bid = "button" + $(this).attr('id');
        $(this).attr('readonly', true);
        $(this).css('width', '65%');
        $(this).after('<button class="filebutton" onclick="' + $(this).attr("onclick") + '" id="button' + $(this).attr('id') + '" type="button">' + $(this).attr('buttonlabel') + '</button>');
        $("#" + bid).css('height', $(this).outerHeight());
        var dif = ($(this).offset().top - $("#" + bid).offset().top) - 1;
        $("#" + bid).css({
            "top": dif + "px"
        });
        $("#" + bid).bind('click', function () {
            $("#" + bid).addClass("filebuttonselected");
            $("#" + tid).focus();
        });
        $("#" + tid).bind('blur', function () {
            $("#" + bid).removeClass("filebuttonselected");
        });
        $("#" + tid).bind('click', function () {
            $("#" + bid).addClass("filebuttonselected");
        });
    });
    
    //$('*').each(function(index) {
    //var onc=$(this).attr('onclick');
    //if (onc){
    //	if (onc.indexOf('goui.')==0){
    //		$(this).attr('onclick', onc.replace(/cgo.ui/,"goui."));
    //	}else
    //if (onc.indexOf('cgo.')==0){
    //	 	$(this).attr('onclick', 'goui.exec(\''+onc.replace(/'/g,"\\'")+'\',this)');
    //}
    //}
    //});
   
});

