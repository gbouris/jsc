(function ($) {

    $.scroller.themes.jqm = {
        defaults: {
            jqmBody: 'c',
            jqmHeader:'b',
            jqmWheel: 'd',
            jqmClickPick: 'c',
            jqmSet: 'b',
            jqmCancel: 'c'
        },
        init: function(elm, inst) {
            var s = inst.settings;
            $('.dw', elm).removeClass('dwbg').addClass('ui-overlay-shadow ui-corner-all ui-body-a');
            $('.dwb-s a', elm).attr('data-role', 'button').attr('data-theme', s.jqmSet);
            $('.dwb-c a', elm).attr('data-role', 'button').attr('data-theme', s.jqmCancel);
            $('.dwwb', elm).attr('data-role', 'button').attr('data-theme', s.jqmClickPick);
            $('.dwv', elm).addClass('ui-header ui-bar-' + s.jqmHeader);
            $('.dwwr', elm).addClass('ui-body-' + s.jqmBody);
            $('.dwpm .dww', elm).addClass('ui-body-' + s.jqmWheel);
            if (s.display != 'inline')
                $('.dw', elm).addClass('pop in');
            elm.trigger('create');
            // Hide on overlay click
            $('.dwo', elm).click(function() { inst.hide(); });
        }
    }

})(jQuery);


$(document).ready(function () {
 if ($("input.datescroller:text").length) {
        $("input.datescroller:text").scroller({
            preset: 'date',
            invalid: {
                daysOfWeek: [0, 6]
            },
            theme: 'jqm',
            display: 'inline',
            mode: 'scroller',
            dateOrder: 'DM ddyy',
            onChange: function (valueText, inst) {
                valueText = jQuery.scroller.formatDate('dd/mm/yyyy', new Date(valueText));
                $(this).val(valueText);
                var edata = 'target:DATESCROLLER' + ';id:' + $(this).attr('id') + ';date:' + valueText;
                //cgo.sendToTop('/cgo/event/'+edata);
            }
        });
    }
});

