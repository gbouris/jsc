(function ($) {

    $.scroller.themes.android = {
        defaults: {
            dateOrder: 'Mddyy',
            mode: 'clickpick',
            height: 50
        }
    }

})(jQuery);

$(document).ready(function () {
 if ($("input.datescroller:text").length) {
        $("input.datescroller:text").scroller({
            preset: 'date',
            invalid: {
                daysOfWeek: [0, 6]
            },
            theme: 'android',
            display: 'inline',
            mode: 'mixed',
            dateOrder: 'DM ddyy',
            onChange: function (valueText, inst) {
                valueText = jQuery.scroller.formatDate('dd/mm/yyyy', new Date(valueText));
                $(this).val(valueText);
                var edata = 'target:DATESCROLLER' + ';id:' + $(this).attr('id') + ';date:' + valueText;
                //cgo.sendToTop('/cgo/event/'+edata);
            }
        });
    }
});
