(function ($) {
    var theme = {
        defaults: {
            dateOrder: 'Mddyy',
            mode: 'mixed',
            rows: 5,
            width: 70,
            showLabel: false
        }
    }

    $.scroller.themes['android-ics'] = theme;
    $.scroller.themes['android-ics light'] = theme;

})(jQuery);


$(document).ready(function () {
 if ($("input.datescroller:text").length) {
        $("input.datescroller:text").scroller({
            preset: 'date',
            invalid: {
                daysOfWeek: [0, 6]
            },
            theme: 'android-ics',
            rows: 5,
            width: 70,
            display: 'inline',
            mode: 'mixed',
            dateOrder: 'DM ddyy',
            onChange: function (valueText, inst) {
                valueText = jQuery.scroller.formatDate('dd/mm/yyyy', new Date(valueText));
                $(this).val(valueText);
                var edata = 'target:DATESCROLLER' + ';id:' + $(this).attr('id') + ';date:' + valueText;
                //cgo.sendToTop('/cgo/event/'+edata);
            }
        });
    }
    
});
