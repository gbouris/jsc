(function ($) {

    $.scroller.themes.ios = {
        defaults: {
            dateOrder: 'dMMyy',
            rows: 5,
            height: 30,
            width: 55,
            headerText: false,
            showLabel: false
        }
    }

})(jQuery);

$(document).ready(function () {
 if ($("input.datescroller:text").length) {
        $("input.datescroller:text").scroller({
            preset: 'date',
            invalid: {
                daysOfWeek: [0, 6]
            },
            theme: 'ios',
            display: 'inline',
            mode: 'scroller',
            dateOrder: 'DM ddyy',
            onChange: function (valueText, inst) {
                valueText = jQuery.scroller.formatDate('dd/mm/yyyy', new Date(valueText));
                $(this).val(valueText);
                var edata = 'target:DATESCROLLER' + ';id:' + $(this).attr('id') + ';date:' + valueText;
                //cgo.sendToTop('/cgo/event/'+edata);
            }
        });
    }
});