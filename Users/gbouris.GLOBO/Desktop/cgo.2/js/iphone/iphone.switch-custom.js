/*
    $(document).ready(function() {
    // Iterate over checkboxes
    $("input[type=checkbox].switch").each(function() {
    // Insert mark-up for switch
    $(this).before(
    '<span class="switch">' +
    '<span class="mask"/><span class="background" cchecked="false" id="'+$(this).attr('id')+'"/>' +
    '</span>'
    );
    
    // Hide checkbox
    $(this).hide();


    // Set inital state
    if (!$(this)[0].checked) {
    $(this).prev().find(".background").css({left: "-56px"});
    $(this).attr('cchecked', true);
    }
    }); // End each()


    // Toggle switch when clicked
    //$("span.switch").click(function() {
    
    
		$("span.switch").bind('touchstart dblclick', function(e){
		    // If on, slide switch off
    e.preventDefault();

    if ($(this).next()[0].checked) {
    $(this).find(".background").animate({left: "-56px"}, 200);
    // Otherwise, slide switch on
    } else {
    $(this).find(".background").animate({left: "0px"}, 200);
    }
    // Toggle state of checkbox
    $(this).next()[0].checked = !$(this).next()[0].checked;
    $(this).find(".background").attr('cchecked', $(this).next()[0].checked);
    
	});
    
    
    }); // End ready()
*/

(function() {
			var $$ = function(selector) {
				return Array.prototype.slice.call(document.querySelectorAll(selector));
			}
			document.addEventListener("DOMContentLoaded", function() {
				var checkbox;
								
				$$(".switch").forEach(function(switchControl) {
				var	divon=$(switchControl).attr('id')+"on";
				var	divoff=$(switchControl).attr('id')+"off";
				var switchData=$(switchControl).children("span").eq(1).attr('id');
		
						checkbox = switchControl.lastElementChild;
						
						var w1=goui.textWidth($('#'+divon).text());
						var w2=goui.textWidth($('#'+divoff).text());
						if (w2>w1){w1=w2;}
						$(switchControl).width(w1+46);
							
					 // Set inital state
					   	if (checkbox.checked) {
						    $('#'+switchData).css({'left' : $(switchControl).width()-28});
							$('#'+divoff).hide();
							$('#'+divon).show();
							switchControl.className = 'switch on switchoff';
						} else {
							$('#'+switchData).css({'left' : 0});
							$('#'+divon).hide();
							$('#'+divoff).show();
							switchControl.className = "switch off switchon";
						}
				
			//	switchData = $(switchControl).children("span");
				
						
					switchControl.addEventListener("dblclick", toggleSwitch, false);
					switchControl.addEventListener("touchstart", toggleSwitch, false);
					
					 function toggleSwitch(e) {
					
						checkbox.checked = !checkbox.checked;
						
						var	divon=$(switchControl).attr('id')+"on";
						var	divoff=$(switchControl).attr('id')+"off";
						var switchData=$(switchControl).children("span").eq(1).attr('id');
						
						    e.preventDefault();
						
							if (checkbox.checked) {
								$('#'+switchData).css({'left' : $(switchControl).width()-28});
								$('#'+divoff).hide();
								$('#'+divon).show();
								switchControl.className = 'switch on switchoff';
							} else {
								$('#'+switchData).css({'left' : 0});
								$('#'+divon).hide();
								$('#'+divoff).show();
								switchControl.className = "switch off switchon";
							}
						
						
						//if (checkbox.checked === true) {
							//if (this.parentNode.textContent) {
								//result.textContent = "You want to " + this.parentNode.textContent.toLowerCase();
							//} else {
							//	result.innerText = "You want to " + this.parentNode.innerText.toLowerCase();
							//}
						//} else {
							//result.textContent = "Off";
							//result.innerText = "Off";
						//}
					}
				});
				
				
		$$(".radioarea").forEach(function(switchControl) {			
				switchControl.addEventListener("touchstart", toggleRadio, false);
				$(switchControl).bind('click', function(e){e.preventDefault();})
				$(switchControl).bind('dblclick', toggleRadio)
		});
		
		function toggleRadio(e){
					var radiobox = $(this).children("input[type=radio]").eq(0);
					$(radiobox).attr("checked", "checked");
		}
				
			}, false);
		})()