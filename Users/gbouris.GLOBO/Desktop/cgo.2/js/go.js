/*
 * UI JavaScript Library v1.0
 *
 * � Copyright George Bouris
 */

var ua = navigator.userAgent;
var browser = {
    iphone: !! ua.match(/(iPhone|iPod)/),
    ipad: !! ua.match(/iPad/),
    blackberry: !! ua.match(/BlackBerry/),
    android: !! ua.match(/Android/),
    windows: !! ua.match(/Windows Phone/),
}

function __IOB_render(content){

	var fcontent = '' + content;
	fcontent = fcontent.replace(/<goscript/g,"<script").replace(/<\/goscript>/g,"</script>");
	eval(fcontent);

	document.write(__GET_IOB());
}

function InvokeHost(sdata){

	var responseJSON = false;
	$.ajax({
	        type: "POST",
	        data: sdata + "&AUTH_KEY=" + __GO_HOST_CONFIG.AUTH_KEY,
	        url: __GO_HOST_CONFIG.HOST,
	        dataType: "json",
	        async: false,
	        success: function(responsejson){
	        	responseJSON = responsejson;
		},
		error: function(obj, status, thrownError){
			if (__GO_HOST_CONFIG.DEBUG)
				alert("A problem has occurred...\n\n" + thrownError);
		}
	});

	return responseJSON;
}

function InvokeAsync (sdata){

	var responseJSON = false;
	return $.ajax({
		type: "POST",
		data: sdata + "&AUTH_KEY=" + __GO_HOST_CONFIG.AUTH_KEY,
		url: __GO_HOST_CONFIG.HOST,
		dataType: "json",
		async: true,
	  	error: function(obj, status, thrownError){
	  		if (__GO_HOST_CONFIG.DEBUG)
	  			alert("A problem has occurred...\n\n" + thrownError);
		}
	});

}

function HandleOnline (cmp, rsdata){
}

function HandleMixed(cmp, rsdata){
}
/*********************************************************************************/
/*******************************************************
 *
 *    GO CLASS
 *
 ********************************************************/

var go = {};

go.db = function (dbname) {
	var databasename = dbname;
	this.close = function(){};
	this.execute = function(sql){

		var sqldata = InvokeHost("action=db.execute&dbname=" + databasename + "&sql=" + sql);
		if (!sqldata){sqldata = {}; sqldata.data = [];}

		function recordset(sqlres){
			this.counter = 0;
			this.eofv = false;
			this.sqldata = sqlres;
			this.length = sqlres.data.length;
			if (this.length == 0){this.eofv = true;}
			this.eof = function(){return this.eofv;}
			this.close = function(){}

			this.nextrow = function(){
				  this.counter++;
					if (this.counter >= this.length){
						this.eofv = true;
						}
			}

			this.get = function(item){
				var obj = this.sqldata.data[this.counter];

				if (isNaN(item)){
					return obj[item];
				}else{
					var cnt = 0;
					for(var k in obj){
		 				   if (item == cnt){
		 				   		return obj[k];
		 				   	}
		 				   cnt++;
					}
				}
			}
		}

		return new recordset(sqldata);
	}


	function recordset(sqlres){
		this.counter = 0;
		this.eofv = false;
		this.sqldata = sqlres;
		this.length = sqlres.data.length;
		if (this.length == 0){this.eofv = true;}
		this.eof = function(){return this.eofv;}
		this.close = function(){}

		this.nextrow = function(){
			  this.counter++;
				if (this.counter >= this.length){
					this.eofv = true;
					}
		}

		this.get = function(item){
			var obj = this.sqldata.data[this.counter];

			if (isNaN(item)){
				return obj[item];
			}else{
				var cnt = 0;
				for(var k in obj){
	 				   if (item == cnt){
	 				   		return obj[k];
	 				   	}
	 				   cnt++;
				}
			}
		}
	}
/************************** NEW Method ***************************************/
	this.executeServer = function(
			method,
			table,
			async,
			params,
			compid,
			methodtype
		){

		var sqldata, rs
		if (((methodtype == "online") && async) || (methodtype == "mixed"))
			InvokeAsync("method=" + method + "&table=" + table + "&params=" + params).done(
				function(sqldata) {
					if (!sqldata) { sqldata = {}; sqldata.data = []; }
					rs = new recordset(sqldata);
					if (methodtype == "online")
						HandleOnline(compid, rs);
					else
						HandleMixed(compid, rs);
					return rs;
				});
		else {
			var sqldata = InvokeHost("method=" + method + "&table=" + table + "&params=" + params);
			if (!sqldata){sqldata = {}; sqldata.data = [];}
			return new recordset(sqldata);
		}
	}
	/*********************************************************************************/
}

go.system = {
	isAndroid: function(){return browser.android;},
	isAndroidTablet: function(){return false;/*TODO*/},
	isIPhone: function(){return browser.iphone;},
	isIPad: function(){return browser.ipad;},
	isWindowsPhone: function(){return browser.windows;},
	isBlackBerry: function(){return browser.blackberry;},
	isWap: function(){return false;/*TODO*/},
	isWeb: function(){return false;/*TODO*/},
	isStudioEmulator: function(){return false;/*TODO*/},
	getOSVersion: function(){return "TODO";/*TODO*/},
	getPlatform: function(){
		if (browser.android){return "Android";}
		else if (browser.isAndroidTablet){return "AndroidTablet";}
		else if (browser.isIPhone){return "iPhone";}
		else if (browser.isIPad){return "iPad";}
		else if (browser.isWindowsPhone){return "Windows";}
		else if (browser.isWap){return "Wap";}
		else if (browser.isWeb){return "Web";}
		else if (browser.isStudioEmulator){return "StudioEmulator";}
		else if (browser.getOSVersion){return "TODO";/*TODO*/}
		else {return "Unknown";}
	}
}

go.utils = {
	htmlEncode: function(value){return $('<div/>').text(value).html()},
	htmlDecode: function(value){return $('<div/>').html(value).text()},
	xmlEncode: function(value){
			var xml_special_to_escaped_one_map = {
				'&': '&amp;',
				'"': '&quot;',
				'<': '&lt;',
				'>': '&gt;'
			};
			return string.replace(/([\&"<>])/g, function(str, item) {
				return xml_special_to_escaped_one_map[item];
			});
		},
	xmlDecode: function(value){
			var escaped_one_to_xml_special_map = {
				'&amp;': '&',
				'&quot;': '"',
				'&lt;': '<',
				'&gt;': '>'
			};
			return string.replace(/(&quot;|&lt;|&gt;|&amp;)/g,
			function(str, item) {
				return escaped_one_to_xml_special_map[item];
				});
			},
	isNumeric: function(value){return !isNaN(value)},
	isAlphaNumeric: function(value){
					var alphaExp = /^[0-9a-zA-Z]+$/;
					if(value.match(alphaExp)){
						return true;
					}else{
						return false;
					}},
	isArray: function(obj){return jQuery.isArray(obj)},
	isDate: function(dateStr){
		var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
		var datePat2 = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})\s+(\d{1,2})(\:|-)(\d{1,2})(\:|-)(\d{1,2})$/;
		var datePat3 = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})\s+(\d{1,2})(\:|-)(\d{1,2})$/;
		var matchArray = dateStr.match(datePat);
		var matchArray2 = dateStr.match(datePat2);
		var matchArray3 = dateStr.match(datePat3);

		if ((matchArray == null) && (matchArray2 == null) && (matchArray3 == null)) {
			return false;
		}

		if (matchArray != null) {
			month = matchArray[3];
			day = matchArray[1];
			year = matchArray[4];
		} else if (matchArray2 != null) {
			month = matchArray2[3];
			day = matchArray2[1];
			year = matchArray2[4];
		} else if (matchArray3 != null) {
			month = matchArray3[3];
			day = matchArray3[1];
			year = matchArray3[4];
		}

		if (month < 1 || month > 12) {
			return false;
		}

		if (day < 1 || day > 31) {
			return false;
		}

		if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
			return false;
		}

		if (month == 2) {
			var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
			if (day > 29 || (day == 29 && !isleap)) {
				return false;
			}
		}

		if (matchArray2 != null) {
		    var hour = matchArray2[5];
		    var mins = matchArray2[7];
		    var secs = matchArray2[9];
		    if ((!this.isNumeric(hour)) || (hour < 0) || (hour > 24)) {
			return false;
		    } else if ((!this.isNumeric(mins)) || (mins < 0) || (mins > 59)) {
			return false;
		    } else if ((!this.isNumeric(secs)) || (secs < 0) || (secs > 59)) {
			return false;
		    }
		} else if (matchArray3 != null) {
		    var hour = matchArray3[5];
		    var mins = matchArray3[7];
		    if ((!this.isNumeric(hour)) || (hour < 0) || (hour > 24)) {
			return false;
		    } else if ((!this.isNumeric(mins)) || (mins < 0) || (mins > 59)) {
			return false;
		    }
		}
		return true;
		}
}

go.getParameter = function(name){
	var gohost = InvokeHost("action=go.getParameter&name="+name);
	if(!gohost)
		return "";

		return gohost.value;
}

go.setGlobal = function(name, value, opts){
	var gohost = InvokeHost("action=go.setGlobal&name="+name+"&value="+value);
	if(!gohost)
		return false;

	return gohost.result;
} //opts: readonly
go.getGlobal = function(name){
	var gohost = InvokeHost("action=go.getGlobal&name="+name);
	if(!gohost)
		return "";

	return gohost.value;
}
go.deleteGlobal = function(name){
	var gohost = InvokeHost("action=go.deleteGlobal&name="+name);
	if(!gohost)
		return false;

	return gohost.result;
}

go.location = {
	getLatitude: function(){getLocation().latitude},
	getLongitude: function(){getLocation().longitude},
	getLocation: function(){
		var gohost = InvokeHost("action=go.location.getLocation");
		if(!gohost){
			var locationdata = {};
			locationdata.latitude = "";
			locationdata.longitude = "";
			return locationdata;
		}
		return gohost;
	}
}

go.timer = function(){};
go.timer.schedule = function(args){/*TODO*/};
//function_name, wait_x_milliseconds, repeat_every_x_milliseconds
//function_name, wait_x_milliseconds, repeat_every_x_milliseconds, makeglobal


go.openPage = function(name, args){
	var params = "";
	for (var key in args) {
  	if (args.hasOwnProperty(key)){
  		params += "&" + key + "=" + args[key];
  	}
	}
	var gohost = InvokeHost("action=go.openPage&name="+name+params);
	if(!gohost)
		return false;

	return gohost.result;
}
