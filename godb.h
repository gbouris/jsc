//
// JavaScript Compiler
//
// � George Bouris
//

#include <v8.h>
#include <iostream>
#include <string>
#include <vector>
#include <boost/unordered_map.hpp>
#include "godef.h"
#include "gosqlite.h"

namespace cgo
{
	namespace db
	{ 
		class Database
		{

		public:
			Database();
			~Database();
			SQLiteDatabase* DB;
			std::vector< std::vector<std::string> > Result;
			int Rowcounter;
			boost::unordered_map<std::string, int> Fields;
		};

		v8::Handle<v8::Value> DBExecute(const v8::Arguments& args);
		v8::Handle<v8::Value> DBClose(const v8::Arguments& args);
		v8::Handle<v8::Value> DBEof(const v8::Arguments& args);
		v8::Handle<v8::Value> DBTotalRecords(const v8::Arguments& args);
		v8::Handle<v8::Value> DBGet(const v8::Arguments& args);
		v8::Handle<v8::Value> DBNextRow(const v8::Arguments& args);
		v8::Handle<v8::Value> DBConstruct(const v8::Arguments& args);
	}

#if ENABLE_LIVE_DEBUG
	extern clock_t T_DB_begin;
	extern double T_DB;
#endif

	extern GOJSCSettings Settings;
}

#define ERR_CLOSED_DATABASE "Cannot use a closed database"
