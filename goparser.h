//
// JavaScript Compiler
//
// � George Bouris
//

#include <v8.h>
#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <iostream>
#include <boost/unordered_map.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp> 
#include <boost/algorithm/string/regex.hpp>
#include "goutils.h"
#include "godebug.h"
#if ENABLE_JS_MINIFY
#include "gojsmin.h"
#endif

namespace cgo
{
	namespace parser
	{
		bool Parse(std::string& source);
		inline void RemoveComments(std::string& source);
		inline void PreserverStringNewLines(std::string& source);
		inline bool irisScript(std::string& source);
		inline void FixOpenCloseTags(std::string& source);
		inline void FinalizeBuffer(std::string& source);
		inline bool PrepareSyntaxTree(std::string& source);
		inline bool PrepareLibrary(const std::string& lib, std::string& source);
		inline void FixEmptyStrings(std::string& source);
		inline void FixEscapedQuotes(std::string& source);
		inline bool FixSlashesInText(std::string& source);
		inline void FixSpaces(std::string& source);
		inline void Clean(std::string& source);
		inline void PreserveSpaces(std::string& psource);
		inline void RemoveSlashComments(std::string& psource);
		inline void RemoveCommentsInScipts(std::string& psource);
		inline void RefactorWrite(std::string& psource);
		inline void IrisScriptLines(std::string& psource);
		inline void FixNewLineSlashes(std::string& psource);
		inline void FixQuotes(std::string& source);
		inline bool IrisScriptParse(std::string& psource);
		inline void PreserveSpacesReverse(std::string& psource);
		bool CheckDuplicateIDs(std::string& psource, std::string& errorstr);
		inline std::string Include(const std::string& filenm, const std::string& path, std::string& errstr);
		inline bool ParseInclude(std::string& psource);
	}

	extern bool ACTIVE_DEBUG;
	extern boost::unordered_map<std::string, int> UNQ_IDS;
	extern boost::unordered_map<std::string, int> UNQ_INCLUDES;
}
