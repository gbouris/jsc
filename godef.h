//
// JavaScript Compiler
//
// � George Bouris
//

#include <stdio.h>
#include <iostream>
#include <string.h>

#pragma once

#define OPENTAG "<@"
#define CLOSETAG "@>"
#define JS_GO_NAMESPACE "go"
#define JS_BUFFER "__IOB"
#define JS_BUFFERFUNC "__GET_IOB"
#define JS_BUFFER_SYNTAX_TREE_TITLE "HTML5"
#define JS_BUFFERFUNC_SYNTAX_TREE_TITLE "PAGE"
#define JS_LIB_EXTENSION ".jslib"
#define JS_LIB_PATH "goinc/"
#define JS_CACHE_PATH "gocache/" 
#define JS_DEBUG_FILE "gojsc.log"
#define JS_INCLUDE_OPEN_TAG "<GOJSIMPORT>"
#define JS_INCLUDE_CLOSE_TAG "</GOJSIMPORT>"
#define JS_INCLUDE_CONFIG_OPEN_TAG "<GOJSCONFIG>"
#define JS_INCLUDE_CONFIG_CLOSE_TAG "</GOJSCONFIG>"
#define JS_INCLUDE_EXPORT_OPEN_TAG "<GOJSEXPORT>"
#define JS_INCLUDE_EXPORT_CLOSE_TAG "</GOJSEXPORT>"
#define JS_BUFFER_PUSH_END_TAG "/*GO:E*/"
#define JS_BUFFER_PUSH_END_TAG_REG "/\\*GO\\:E\\*/"
#define JS_DEBUG_LINE_OPENTAG "/*["
#define JS_DEBUG_LINE_CLOSETAG "]*/"
#define JS_DEBUG_LINE_REGEX "/\\*\\[([0-9]+)\\]\\*/"
#define JS_DEBUG_LINE_REGEX_OPTIONAL "/*\\**\\[*([0-9]*)\\]*\\**/*"
#define JS_DEBUG_LINE_EMPTY JS_DEBUG_LINE_OPENTAG JS_DEBUG_LINE_CLOSETAG
#define JS_DEBUG_GO_LINE_OPENTAG "<!--#GOBEGIN"
#define JS_DEBUG_GO_LINE_CLOSETAG "<!--#GOEND-->"
#define JS_DEBUG_GO_LINE_OPENTAG_REG "<\\!--#GOBEGIN"
#define JS_DEBUG_GO_LINE_REGEX "<!--#GOBEGIN\\s*lines:(.*?);"
#define JS_PRINT_COMMANDS {"document.write","write","print"}

#define ARRLEN(s) (sizeof(s)/sizeof(s[0]))
#define STRLEN(s) ARRLEN(s) - 1
#define STR_EXPAND(tok) #tok
#define STR(tok) STR_EXPAND(tok)

#define ENABLE_LIVE_DEBUG 1
#define ENABLE_CACHE 1
#define ENABLE_JS_MINIFY 1

typedef struct{
 				bool PRESERVE_SPACES;
				bool CHECK_DUPLICATE_IDS;
				bool RETURN_SOURCE;
				bool EXECUTE_ONLY;
				bool RETURN_XML_ERROR;
				bool IS_WEB;
				bool RETURN_SYNTAX_TREE;
#if ENABLE_LIVE_DEBUG
				bool DEBUG_TO_FILE;
				bool DEBUG_TIME;
				bool DEBUG_TIME_SPLIT;
				bool DEBUG_TIME_WITH_PRINT;
#endif
#if ENABLE_CACHE
				bool ENABLE_ACTIVE_CACHE;
				bool RESET_CACHE;
#endif
				std::string BASE_PATH;
				std::string LIB_PATH;
				std::string USR_LIB_PATH;
#if ENABLE_CACHE
				std::string CACHE_PATH;
#endif
	}GOJSCSettings;


