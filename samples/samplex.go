<!DOCTYPE html>
<@ include("go.encryption.aes"); @>
<@
var dbconn="C:/Users/George/Desktop/gojsc++/Chinook_Sqlite.sqlite";
@>
<html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/go.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(16,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();
 
function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html>

<html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				
<@
showDB();

function showDB(){
	
	var oconn = new go.db(dbconn);
	var rs=oconn.execute("SELECT B.Name as bname,A.Title,C.Name as cname,C.TrackId FROM Album A inner join Artist B on A.ArtistId=B.ArtistId  inner join Track C on  A.AlbumId=C.AlbumId order by B.Name");
	
	   var res0,res1;
	   var ses0,ses1;
	   var sc=1;
	   while(!rs.eof())
	                {
	                		  ses0=rs.get(0);
	                		  ses1=rs.get(1);
	                		 
	                		  if (res0!=ses0 || res1!=ses1){
	                		  	res0=ses0;
	                		  	res1=ses1;
	                		  	if (sc>1){
	                		  		write("</ul></div></div><BR><BR>");
	                		  		}
	                		  	write("\n<label>");
	                		  	write(res0+" - ");
	                		  	write(res1);
	                		  	write("</label><BR><div class=\"spacer\"><div id=\"list1\" class=\"list\" style=\"width:99%\"><ul>\n");
	                		  	sc=1;
	                		  }
	                		   @>
	     										 <li onclick="alert('ID::<@=rs.get(3);@>')"><table style="padding:0px;margin:0px;"><tr><td style="width:32px"><img src="play.png" style="position:relative;top:3px"></td><td><@=rs.get(2);@></td></tr></table></li>
	                			<@			
	                		 sc++;
	                	
	                   rs.nextrow();
	                }
	                
	
	oconn.close();
	}

@>
</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html><html>		
			<head>
				<meta name = "viewport" content="width=device-width, initial-scale=1 user-scalable=no">
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <link href="skins/skin1/iphone/cgo-iphone-1.css" rel="stylesheet" type="text/css">
				<script type="text/javascript" src="js/jquery-1.8.1.js"></script>
				<script type="text/javascript" src="js/jquery.touchwipe-custom-1.1.1.js"></script>
				<script type="text/javascript" src="js/iphone/iphone.switch-custom.js"></script>
				<script type="text/javascript" src="js/jquery.tablesorter-custom.js"></script>
				<script type="text/javascript" src="js/jquery.smartpaginator-custom.js"></script>
				<script type="text/javascript" src="js/jquery.uitablefilter-custom.js"></script>
				<script type="text/javascript" src="js/cgoui.js"></script>
			</head>
			
				  
<body>
<div style="width:640px;">
<h2>1. iPhone like UI</h2>
<div class="finput"><div class="finput-label"><label>Active</label></div><div class="finput-input"><div class="switch on" id="s2"><span class="switchon" id="s2on"><span class="switchontext">ON</span></span><span class="switchdata" id="s2s"></span><span class="switchoff" id="s2off">OFF</span><input type="checkbox" id="chckbx2" checked/></div></div></div>
<fieldset>
		    <legend>Choose a Car</legend>	
			  		<div class="finput"><div class="finput-label"><label>Car</label></div>
        			<div class="finput-input"> 	
        				<div class="toggle-buttons">
          	 			<label class="radioarea"><input name="radio-button-group-0" id="radio-0" value="c1" type="radio"><span></span><span class="radiolabel">Audi</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-1" value="c2" checked type="radio"><span></span><span class="radiolabel">BMW</span></label>
	              	<label class="radioarea"><input name="radio-button-group-0" id="radio-2" value="c3" type="radio"><span></span><span class="radiolabel">Mercedes-Benz</span></label>
              </div>
             </div>
        		</div>
      	
		    
		  </fieldset>
		<BR><BR> 

<h2>2. GO! JavaScript Programming</h2>
<fieldset>
<legend>2.1 Custom Local Functions & Include Libraries</legend>
<BR><span style="font-size:12px"><b>Generate Random Keys</b> - Local Function <code><b><font color="#336699">&lt;@</font></b> var key=makekey(); function makekey(){... <b><font color="#336699">@&gt;</font></b></code></span>
<BR><span style="font-size:12px"><b>AES 256 Encryption</b> - Include Library <code><b><font color="#336699">&lt;@</font></b> include("go.encryption.aes"); <b><font color="#336699">@&gt;</font></b></code></span>
<BR><BR>
<@ 

for (var o=0;o<3;o++){ 
var plaintext = makekey(24,4);
var password="lala";
var ciphertext = Aes.Ctr.encrypt(plaintext, password, 256);
	print("<span style=\"font-size:13px\"><b>Random Key:</b><BR>"+plaintext+"<BR><b>Encrypted:</b><BR>"+ciphertext+"</span><BR><BR>");
 }

@>
</fieldset>

<BR><BR>
<h2>3. Get Data From SQLite Database</h2>

<h3>3.1 Get Customers (Grid)</h3>

			
			<table id="grid1" class="datatable" border="1">
			<thead>
				<tr>
					<th width="0%"><img src="images/cgo_32.png"></th>
					<th width="50%">Name</th>
					<th width="50%">LastName</th>
			 </tr>
			 <tr><td colspan="50"><div class="searcharea"><input type="searchbox" onkeyup="cgoui.filter('grid1', 'grid1-paginator', 3, this)"><span class="icon_clear">x</span></div></td></tr>
			</thead>
			
			<@
			var oconn = new go.db(dbconn);
		  var rs=oconn.execute("SELECT CustomerId,FirstName,LastName FROM Customer order by LastName");
		  while(!rs.eof()){

	    @>
	            <tr style="cursor:pointer;" onclick="alert('cgo.openpage(CGO.Details, <@=rs.get("CustomerId");@>)')">
								<td style="text-align:center"><img src="images/contact.png"></td>	
								<td><@=rs.get("FirstName");@></td>
								<td><@=rs.get("LastName");@></td>
						 	</tr>	  
			<@
				rs.nextrow();
	       }	
	       
	      oconn.close();
			@>
			
						
					</table>
					<div id="grid1-paginator"></div>
					<script type="text/javascript">
						cgoui.paginate("grid1", "grid1-paginator", 5); 
					</script>
					<BR><BR>
<h3>3.2 Get Artists,Albums and Tracks (List)</h3>	
<BR>				

</div>
<BR><BR>
 
</body>
</html>
<@

 

function makekey(digits,splinum)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
        
    for( i=0; i < digits; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

	  digits+=(digits/splinum)-1;
    for ( i=splinum; i < digits; i+=splinum+1)
    		text=splice(text,i,0,"-");
	  
    return text;
}

function splice(str, idx, rem, s ) {
    return (str.slice(0,idx) + s + str.slice(idx + Math.abs(rem)));
}



@>