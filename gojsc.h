//
// JavaScript Compiler
//
// � George Bouris
//

#include <v8.h>
#include "godef.h"
#include "goversion.h"
#include "goutils.h"
#include "godb.h"
#include "goparser.h"
#include "godebug.h"

#if ENABLE_CACHE || ENABLE_LIVE_DEBUG
#include <boost/thread.hpp>
#endif

namespace cgo
{
	std::string Version();
	void ShowVersion();
	inline void InitV8(v8::Persistent<v8::Context>& context);
	bool ExecuteScriptFile(const char* name);
	inline char* GetParameteres(int argc, char* argv[]);
	v8::Handle<v8::Value> Print(const v8::Arguments &args);
	v8::Handle<v8::Value> GetParameter(const v8::Arguments &args);
	inline void ResetCounters();
	inline void CheckDuplicateIDs(std::string& scriptresult);
	bool ExecuteString(const v8::Handle<v8::String> source, std::string& scriptresult);	
	extern "C" __declspec(dllexport) const char* Compile(
														const char* scriptsource, 
														const char* original_source,
														int& ref_haserrors, 
														const char* base_path, 
														const char* usr_lib_path,
														const bool return_xml_errror, 
														const bool return_source,
														const bool return_syntax_tree, 
														const bool preserve_spaces, 
														const bool check_duplicate_ids,
														const bool execute_only
#if ENABLE_CACHE
														,const bool enable_active_cache
#endif
														);

#if ENABLE_CACHE
	inline bool HandleCacheRead(std::string& cname, std::string& content);
	inline void HandleCacheWrite(std::string& cname, const std::string& content);
#endif

#if ENABLE_LIVE_DEBUG
	clock_t T_begin = 0;
	clock_t T_V8_begin = 0;
	clock_t T_DB_begin = 0;
	clock_t T_PRINT_begin = 0;
	double T_DB = 0;
	double T_V8 = 0;
#endif

	bool ACTIVE_DEBUG = false;
	boost::unordered_map<std::string, int> UNQ_IDS;
	boost::unordered_map<std::string, int> UNQ_INCLUDES;
	const char* JS_SRC = NULL;

	GOJSCSettings Settings = {
								false,
								false,
								true,
								false,
								false,
								false,
								false,
#if ENABLE_LIVE_DEBUG
								false,
								false,
								false,
								false,
#endif
#if ENABLE_CACHE
								false,
								false,
#endif
								"",
								JS_LIB_PATH,
								"",
#if ENABLE_CACHE
								JS_CACHE_PATH,
#endif
							  };
}



