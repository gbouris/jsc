//
// JavaScript Compiler
//
// � George Bouris
//

#include <v8.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include "godef.h"
#if ENABLE_CACHE
#include <boost/crc.hpp>
#endif


namespace cgo
{
	namespace utils
	{
		bool IsNumeric(const char* str);
		char* StringToChar(const std::string& str);	
		int Index(const char* str1, const char* str2);
		std::string GetMatch(const std::string& base, const std::string& match);
		std::string XMLEncode(std::string& data);
		void MinifySimple(std::string& source);
		std::string FormatError(const std::string& errstr);
#if ENABLE_CACHE
		int GetCrc32(const std::string& my_string);
#endif
#if ENABLE_LIVE_DEBUG
		double Diffclock(const clock_t& clock1, const clock_t& clock2);
#endif
	}

	extern GOJSCSettings Settings;
}
