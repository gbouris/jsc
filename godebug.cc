//
// JavaScript Compiler
//
// � George Bouris
//

#include "godebug.h"

using namespace cgo::debug;

std::string cgo::debug::ReportException(const v8::TryCatch* try_catch) 
{	
	if (!ACTIVE_DEBUG && !Settings.EXECUTE_ONLY)
	 return "";

	v8::HandleScope handle_scope;
	v8::String::Utf8Value exception(try_catch->Exception());
	std::string result;
	std::string jsexacterrpart("");
  
	v8::Handle<v8::Message> message = try_catch->Message();

	if (Settings.EXECUTE_ONLY)
	{
		if (message.IsEmpty()) 
			result.append(*exception);
		else 
		{
			result.append(*exception);
			result.append("\n");
			result.append(*v8::String::Utf8Value(try_catch->Message()->GetSourceLine()));
		}
		return result;
	}

	if (message.IsEmpty()) 
		result.append(*exception);
	else 
	{ 
	if (Settings.IS_WEB)
		result.append("<span style=\"color:red\">");

	result.append(*exception);

	if (Settings.IS_WEB)
		result.append("</span>");

	result.append("\n");

	std::string sourceline = std::string(*v8::String::Utf8Value(message->GetSourceLine()));
		
	int serrpos = message->GetStartColumn();
	if (serrpos > 1)
		serrpos--;

	int eerrpos =  message->GetEndColumn() - message->GetStartColumn() + 2;
	int sl = sourceline.length();
	while (serrpos + eerrpos > sl)
		--eerrpos;
	
	jsexacterrpart = sourceline.substr(serrpos, eerrpos);
	
	int start = message->GetStartColumn();
	int nstart = sourceline.substr(0, start).rfind(JS_DEBUG_LINE_OPENTAG);
	int stend = sourceline.substr(start).find(JS_DEBUG_LINE_OPENTAG, STRLEN(JS_DEBUG_LINE_OPENTAG));
	if (stend < 0)
		stend = sourceline.length() - start;
	
	int end = start + stend;

	for(int i = nstart; i < end; i++) 
		result += sourceline[i];
	}
  
	return HandleError(result, jsexacterrpart);
}

std::string cgo::debug::HandleError(const std::string& error, const std::string& jsexacterrpart)
{	
	int splt = utils::Index(error.c_str(), "\n");
	
	std::string errorresult;
	errorresult.append(error.substr(0, splt));
	errorresult.append("\n");

	std::string errpart = error.substr(splt + 1);
	
	std::string linenumber;
	boost::smatch result;
	boost::regex pattern(JS_DEBUG_LINE_REGEX, boost::regex_constants::perl);

	if (boost::regex_search(errpart, result, pattern)) 
	{
		linenumber = result[1];
		if (Settings.IS_WEB)
			errorresult.append("<BR>");
		errorresult.append("Line: ");
		errorresult.append(linenumber);
		errorresult.append("\n");
		if (Settings.IS_WEB)
			errorresult.append("<BR><span style=\"color:#777;\"><i>");
		
		int errcol = -1; 
		int errlength = -1;

		errpart = GetSourceCodeLine(atoi(linenumber.c_str()), errpart, jsexacterrpart, errcol, errlength);

		if (errcol > -1)
		{
			if (Settings.IS_WEB)
				errorresult.append("<BR>");
			errorresult.append("Col: ");

			char lbuffer[33];
			itoa(errcol,lbuffer,10);
			errorresult.append(lbuffer);
			
			errorresult.append("\n");
			if (Settings.IS_WEB)
				errorresult.append("<BR>");
		}

		if (errlength > -1)
		{
			if (Settings.IS_WEB)
				errorresult.append("<BR>");
			errorresult.append("Length: ");

			char lbuffer[33];
			itoa(errlength,lbuffer,10);
			errorresult.append(lbuffer);
			
			errorresult.append("\n");
			if (Settings.IS_WEB)
				errorresult.append("<BR>");
		}
	}

	if (errpart.length() > 0)
	{
		errorresult.append(errpart);

		if (Settings.IS_WEB)
			errorresult.append("</i></span>");
	}
	else
	{
		int linevalue = atoi(linenumber.c_str());
		linevalue--;
		char lbuffer[33];
		itoa(linevalue,lbuffer,10);
		errorresult.append(lbuffer);
	}

	if (Settings.IS_WEB)
			errorresult.append("</span>");
	
	return errorresult;	
}

inline std::vector<std::string> cgo::debug::CleanJSError(std::string& jserrormsg)
{
	std::vector<std::string> retv;

	jserrormsg.erase(0, utils::Index(jserrormsg.c_str(), JS_DEBUG_LINE_CLOSETAG) + STRLEN(JS_DEBUG_LINE_CLOSETAG));
		
	if (jserrormsg.length() > 0)
	{
		int checkstr = utils::Index(jserrormsg.c_str(), JS_BUFFER);
		if (checkstr > -1)
		{
			std::string rej = JS_BUFFER;
			rej.append("\\.*[push]*\\(*");
			boost::regex patternj(rej, boost::regex_constants::perl);
			jserrormsg = boost::regex_replace(jserrormsg, patternj, "");

			boost::erase_all(jserrormsg, "return join('');");
		} 
		
		boost::regex pattern(JS_DEBUG_LINE_REGEX, boost::regex_constants::perl);
		jserrormsg = boost::regex_replace(jserrormsg, pattern, "");
		
		boost::trim(jserrormsg);
		
		int pos = utils::Index(jserrormsg.c_str(), "');");
		if (pos > -1)
			jserrormsg.erase(0, pos + 3);

		if (jserrormsg[0] == '/' && jserrormsg[1] == '*')
			jserrormsg.erase(0,2);
		else if (jserrormsg[0] == '\'' && jserrormsg[1] == ')' && jserrormsg[2] == ';')
			jserrormsg.erase(0,3);

		int sz;
		char h;
		bool found = true;

		while(found)
		{
			sz = jserrormsg.length();
			h = jserrormsg[sz - 1];
			switch (h)
			{
			case '/':
				jserrormsg.erase(sz -1 ,1);
				break;
			case '*': 
				jserrormsg.erase(sz -1, 1);
				break;
			case '}':
				jserrormsg.erase(sz -1, 1);
				break;
			case '\r':
				jserrormsg.erase(sz -1 ,1);
				break;
			default: found = false;
			}
		}

		if (jserrormsg[0] == ';')
			jserrormsg.erase(0 ,1);
		
		boost::trim(jserrormsg);

		retv.push_back(jserrormsg);

		if (cgo::utils::Index(jserrormsg.c_str(), "\\\"") > -1)
		{
			boost::replace_all(jserrormsg, "\\\"", "\"");
			retv.push_back(jserrormsg);
		}
	}
	else
	retv.push_back(jserrormsg);
	
	return retv;
}

inline std::string cgo::debug::GetSourceCodeLine(int srcline, std::string& errpart, const std::string& jsexacterrpart, int& errcol, int& errlength)
{
	std::vector<std::string> psbltext = CleanJSError(errpart);
	std::string errmsg;
	std::stringstream ss;
	std::string sline;
	int line;

	int sz = psbltext.size();

	for (int i  = 0; i < sz; i++)
	{
		errmsg = psbltext[i];
		ss.str(JS_SRC);
		sline;
		line = 1;
	
		while(std::getline(ss, sline))
		{
			if (line == srcline)
			{
				boost::trim(sline);
				
				int pos = cgo::utils::Index(sline.c_str(), errmsg.c_str());

				if (pos > -1)
				{
					int ipos = cgo::utils::Index(sline.c_str(), jsexacterrpart.c_str());
					if (ipos > -1)
						pos = ipos;

					if (Settings.IS_WEB)
					{
						boost::replace_all(sline, "<", "&lt;");
						boost::replace_all(sline, ">", "&gt;");
					}

					sline.append("\n");
					if (Settings.IS_WEB)
						sline.append("<BR><span style=\"color:#7F9FBF;font-family:Courier\">");
				
					for (int i = 0; i < pos; i++)
					{
						if (Settings.IS_WEB)
							sline.append("&nbsp;");
						else
							sline.append(" ");
					}
					
					int errend = jsexacterrpart.length();

					errcol = pos;
					errlength = errend;

					for (int i = 0; i < errend; i++)
						sline.append("~");

					if (Settings.IS_WEB)
						sline.append("</span>");

					return sline;
				}
				else
				{
					if (Settings.IS_WEB)
					{
						psbltext[0].insert(0, "<BR><span style=\"color:#bbb;text-decoration:italic;font-size:14px\">");
						psbltext[0].append("</span>");
					}
					psbltext[0].insert(0, "\n");
					psbltext[0].insert(0, sline);
					return psbltext[0];
				}
			}
			line++;
		}
	}
	
	return "";
}

inline std::string cgo::debug::FixDebugComments2(std::string& psource)
{
	std::stringstream ss(psource);
	std::string retcontent;
	std::string sline;

	while(std::getline(ss, sline))
	{
		std::string x;
		if (sline != "")
			x = "/**/\r\n";
		
		retcontent.append(x);
	} 

	retcontent = retcontent.substr(0,retcontent.length()-2);	

	return retcontent;
}

inline void cgo::debug::FixDebugComments(std::string& psource)
{	
	std::string xcontent = psource;
	boost::regex pattern("\\/\\*(.*?)\\*\\/", boost::regex_constants::perl);
	boost::sregex_token_iterator it(xcontent.begin(), xcontent.end(), pattern, 1);
	boost::sregex_token_iterator end;
	
	for(; it != end; ++it)
	{
		std::string x = it->str();
			
		std::string orig = "/*";
		orig.append(x);
		orig.append("*/");

		x = FixDebugComments2(x);
			
		boost::replace_first(psource, orig, x);
	}	
}

std::string cgo::debug::PrepareLines(std::string& psource)
{
	FixDebugComments(psource);
	std::stringstream ss(psource);
	std::string retcontent;
	int line = 1;
	std::string sline;
	char lbuffer[33];
	bool linecounterpause = false;

	while(std::getline(ss, sline))
	{
		std::string x;
		if (sline != "")
		{
			if (utils::Index(sline.c_str(), JS_DEBUG_GO_LINE_OPENTAG) > -1)
			{
				std::string objlines = utils::GetMatch(sline,JS_DEBUG_GO_LINE_REGEX);
				if (utils::IsNumeric(objlines.c_str()))
					line += atoi(objlines.c_str()) - 1;
				linecounterpause = true;
			}
			 
			if (utils::Index(sline.c_str(), JS_DEBUG_GO_LINE_CLOSETAG) > -1)
				linecounterpause = false;

			x = JS_DEBUG_LINE_OPENTAG;
			itoa(line,lbuffer,10);
			x.append(lbuffer);
			x.append(JS_DEBUG_LINE_CLOSETAG);
			x.append(" ");
			x.append(sline);
		}
		retcontent.append(x);
		if (!linecounterpause)
			line++;
	} 
	
	return retcontent;
}

std::string cgo::debug::PrepareLinesIncludes(std::string& psource, const std::string& line)
{
	FixDebugComments(psource);

	if (line.empty())
		return psource;

	std::stringstream ss(psource);
	std::string retcontent;
	std::string sline;

	while(std::getline(ss, sline))
	{
		std::string x;
		if (sline != "")
		{
			x = JS_DEBUG_LINE_OPENTAG;
			x.append(line);
			x.append(JS_DEBUG_LINE_CLOSETAG);
			x.append(" ");
			x.append(sline);
		}
		retcontent.append(x);	
	}
	
	return retcontent;
}

std::string  cgo::debug::ErrorToXML(const std::string& error)
{
	std::string res = "<?xml version=\"1.0\"?>\n<error>\n";
	std::vector<std::string> strs;
	boost::split(strs, error, boost::is_any_of("\n"));
	
	int sz = strs.size();
	
	if (sz >= 5)
	{
		for(int i = 0; i < sz; i++)
		{
			switch (i)
			{
			case 0:
				res.append("<descr>");
				res.append(utils::XMLEncode(strs[i]));
				res.append("</descr>\n");
				break;
			case 1:
				res.append("<line>");
				boost::erase_all(strs[i],"Line: ");
				res.append(utils::XMLEncode(strs[i]));
				res.append("</line>\n");
				break;
			case 2:
				res.append("<col>");
				boost::erase_all(strs[i],"Col: ");
				res.append(utils::XMLEncode(strs[i]));
				res.append("</col>\n");
				break;
			case 3:
				res.append("<length>");
				boost::erase_all(strs[i],"Length: ");
				res.append(utils::XMLEncode(strs[i]));
				res.append("</length>\n");
				break;
			case 4:
				res.append("<src>");
				while (i < sz)
				{
					res.append(utils::XMLEncode(strs[i]));
					if (i > 2)
						res.append("\n");
					i++;
				}
				res.append("</src>\n");
				break;
			}
		}
	}
	else
	{
		res.append("<descr>");
		res.append(error);
		res.append("</descr>\n");
	}
    
	res.append("</error>\n");

	return res;
}

#if ENABLE_LIVE_DEBUG
void cgo::debug::WriteDebugFile(const std::string content)
{
	FILE *file;
	if (file=fopen(JS_DEBUG_FILE, "wb"))
	{
		fprintf(file, "%s", content.c_str());
		fclose(file);
	}
	else
		std::cout << "Can not write to " << JS_DEBUG_FILE << std::endl;
} 
#endif
