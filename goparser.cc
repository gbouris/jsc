//
// JavaScript Compiler
//
// � George Bouris
//

#include "goparser.h"
#include "goexceptions.h"

using namespace cgo::parser;

bool cgo::parser::Parse(std::string& source)
{
	if (!ACTIVE_DEBUG)
		RemoveComments(source);
	
	PreserverStringNewLines(source);
	
	if (Settings.PRESERVE_SPACES)
		PreserveSpaces(source);
			
	if (!irisScript(source))
		return false;
	
	FixOpenCloseTags(source);
	FinalizeBuffer(source);
	FixSpaces(source);
	FixEmptyStrings(source);
	FixEscapedQuotes(source);
	
	if (FixSlashesInText(source))
		return false;
	
	Clean(source);

	if (Settings.RETURN_SYNTAX_TREE && !ACTIVE_DEBUG)
		if (!PrepareSyntaxTree(source))
			return false;

#if ENABLE_JS_MINIFY
	if (!Settings.RETURN_SYNTAX_TREE && !ACTIVE_DEBUG)
	{
		try{
			minifier::Minify(source);	
		}catch(const GOMinException&){
			utils::MinifySimple(source);
		}
	}
#else
	if (!Settings.RETURN_SYNTAX_TREE && !ACTIVE_DEBUG)
		utils::MinifySimple(source);
#endif

	return true;
}

inline void cgo::parser::Clean(std::string& source)
{
	boost::erase_all(source, JS_BUFFER_PUSH_END_TAG);

	std::string re = JS_DEBUG_GO_LINE_OPENTAG_REG;
	re.append("(.*?)-->");
	boost::regex pattern(re, boost::regex_constants::perl);
	source = boost::regex_replace(source, pattern, "");

	boost::erase_all(source, JS_DEBUG_GO_LINE_CLOSETAG);
}

inline void cgo::parser::RemoveComments(std::string& source)
{	
	boost::regex pattern("\\/\\*(.*?)\\*\\/", boost::regex_constants::perl);
	source = boost::regex_replace(source, pattern, "");
}

inline void cgo::parser::PreserverStringNewLines(std::string& source)
{
	boost::replace_all(source, "\\n", "\\\\n");
}

inline void cgo::parser::PreserveSpaces(std::string& source)
{
	boost::replace_all(source, "\n", "\\n");
}

inline bool cgo::parser::irisScript(std::string& source)
{
	std::string xcontent = source;
	std::string re = OPENTAG;
	re.append("(.*?)");
	re.append(CLOSETAG);
	
	boost::regex expression(re, boost::regex::perl);

	boost::sregex_token_iterator it(xcontent.begin(), xcontent.end(), expression, 1);
	boost::sregex_token_iterator end;

	std::string x;
	std::string px;

	for(; it != end; ++it)
	{		
		x = OPENTAG;
		x.append(it->str());
		x.append(CLOSETAG);
		
		px = x;
		if (Settings.PRESERVE_SPACES)
			PreserveSpacesReverse(x);
		
		if (!IrisScriptParse(x))
			return false;
	
		boost::replace_first(source, px, x);	
	}
	
	return true;
}

inline void cgo::parser::FixSpaces(std::string& source)
{
	std::string renl;
	if (Settings.PRESERVE_SPACES)
		renl = "\\s";
	else
		renl = "\\s+";

	std::string nl = "\\";
	nl.append("__NEWL__");

	std::string xcontent = source;
	std::string re = JS_BUFFER;
	re.append("\\.push\\((.*?)\\);");
	re.append(JS_BUFFER_PUSH_END_TAG_REG);
	
	boost::regex expression(re, boost::regex::perl);
	
	boost::sregex_token_iterator it(xcontent.begin(), xcontent.end(), expression, 1);
	boost::sregex_token_iterator end;
	
	std::string x;
	std::string px;

	for(; it != end; ++it)
	{	
		x = it->str();
	
		boost::replace_all(x, "\\\n", nl);

		boost::regex pattern(renl, boost::regex_constants::perl);
		std::string mt = " ";
		x = boost::regex_replace(x, pattern, mt);

		boost::replace_all(x, nl, "n");

		boost::replace_all(source, it->str(), x);
	}
}

inline void cgo::parser::IrisScriptLines(std::string& psource)
{
	std::string res;
	std::vector<std::string> strs;
	boost::split(strs, psource, boost::is_any_of(";"));
	size_t sz = strs.size();

	char* printcomds[] = JS_PRINT_COMMANDS;
	
	for(size_t i = 0; i < sz; i++)
	{
		strs[i].append(";");

		for (int o = 0; o < ARRLEN(printcomds); o++)
		{
			if (utils::Index(psource.c_str(), printcomds[o]) > -1)
			{
				RefactorWrite(strs[i]);
				break;
			}
		}
		res.append(strs[i]);
	}
    res = res.substr(0, res.length() - 1);
    
	psource = res;
}

inline void cgo::parser::FixOpenCloseTags(std::string& source)
{
	std::string re = OPENTAG;
	boost::regex pattern(OPENTAG, boost::regex_constants::perl);
	std::string mt = "\\'\\)\\;";
	mt.append(JS_BUFFER_PUSH_END_TAG);
	mt.append(OPENTAG);
	source = boost::regex_replace(source, pattern, mt);
	
	re = CLOSETAG;
	pattern.assign(re, boost::regex_constants::perl);
	mt = CLOSETAG;
	mt.append(JS_BUFFER);
	mt.append("\\.push\\(\\'");
	source = boost::regex_replace(source, pattern, mt);

	re = OPENTAG;
	re.append("=(.*?);");
	pattern.assign(re, boost::regex_constants::perl);
	mt = OPENTAG;
	mt.append(JS_BUFFER);
	mt.append("\\.push\\($1\\);");
	mt.append(JS_BUFFER_PUSH_END_TAG);
	source = boost::regex_replace(source, pattern, mt);

	re = OPENTAG;
	re.append("(.*?)");
	re.append(CLOSETAG);
	pattern.assign(re, boost::regex_constants::perl);
	mt = "$1 ";
	source = boost::regex_replace(source, pattern, mt);

	re = JS_BUFFER;
	re.append("\\.push\\(\\'' \\'\\);");
	re.append(JS_BUFFER_PUSH_END_TAG_REG);
	re.append(CLOSETAG);
	pattern.assign(re, boost::regex_constants::perl);
	mt = "";
	source = boost::regex_replace(source, pattern, mt);
}

inline void cgo::parser::FinalizeBuffer(std::string& source)
{
	std::string mp = "function ";
	mp.append(JS_BUFFERFUNC);
	mp.append("(){ ");
	mp.append("var ");
	mp.append(JS_BUFFER);
	mp.append("=[]; ");
	mp.append(JS_BUFFER);
	mp.append(".push('");
	source.insert(0,mp); 

	source.append("');");
	source.append(JS_BUFFER_PUSH_END_TAG);
	source.append(" return ");
	source.append(JS_BUFFER);
	source.append(".join(''); }");
	
	if (!Settings.RETURN_SYNTAX_TREE)
	{
		source.append("\nprint(");
		source.append(JS_BUFFERFUNC);
		source.append("());");
	}
}

inline bool cgo::parser::PrepareLibrary(const std::string& lib, std::string& source)
{
	std::string errstr;
	std::string xinc = Include(lib, Settings.LIB_PATH, errstr);
	
	if (xinc.empty())
	{
		std::string error_msg = errstr;
		error_msg.append(lib);
		throw GOLocalException(error_msg);

		return false;
	}
	else
	{
		source.append("\n");
		source.append(xinc);
		source.append("\n");
	}

	return true;
}

inline bool cgo::parser::PrepareSyntaxTree(std::string& source)
{
	if (PrepareLibrary("go.esprima.jslib", source))
	{
		boost::replace_all(source, JS_BUFFERFUNC, JS_BUFFERFUNC_SYNTAX_TREE_TITLE);
		boost::replace_all(source, JS_BUFFER, JS_BUFFER_SYNTAX_TREE_TITLE);
		source.append("\nvar __IOB_Syntax = esprima.parse(");
		source.append(JS_BUFFERFUNC_SYNTAX_TREE_TITLE);
		source.append(");\nprint(JSON.stringify(__IOB_Syntax, null, 2));\n");

		return true;
	}

	return false;
}

inline void cgo::parser::FixEmptyStrings(std::string& source)
{
	std::string re = JS_BUFFER;
	re.append("\\.push\\(''\\);");
	re.append(JS_BUFFER_PUSH_END_TAG_REG);

	boost::regex pattern(re, boost::regex_constants::perl);
	source = boost::regex_replace(source, pattern, "");

	re = JS_BUFFER;
	re.append("\\.push\\(' '\\);");
	re.append(JS_BUFFER_PUSH_END_TAG_REG);

	pattern.assign(re, boost::regex_constants::perl);
	source = boost::regex_replace(source, pattern, "");
}

inline void cgo::parser::FixEscapedQuotes(std::string& source)
{
	boost::replace_all(source, "\\\\\"", "\\\"");
}

inline bool cgo::parser::FixSlashesInText(std::string& psource)
{	
	std::string xcontent = psource;
	std::string re = JS_BUFFER;
	re.append("\\.push\\('(.*?)'\\);");
	re.append(JS_BUFFER_PUSH_END_TAG_REG);
	
	boost::regex expression(re, boost::regex::perl);
	
	boost::sregex_token_iterator it(xcontent.begin(), xcontent.end(), expression, 1);
	boost::sregex_token_iterator end;
	
	std::string x;
	std::string px;

	for(; it != end; ++it)
	{	
		x = it->str();
		
		px = JS_BUFFER;
		px.append(".push('");
		px.append(it->str());
		px.append("');");
		
		boost::replace_all(x, "'", "\\'");
		boost::replace_all(x, "\\\\'", "\\\\\\'");
		
		std::string x2 = JS_BUFFER;
		x2.append(".push('");
		x2.append(x);
		x2.append("');");
		boost::replace_first(psource, px, x2);
	}

	return false;
}

inline void cgo::parser::RemoveSlashComments(std::string& psource)
{	
	boost::regex pattern("//(?!\\*)(.*?)\\\r", boost::regex_constants::perl);
	psource = boost::regex_replace(psource, pattern, "");
}

inline void cgo::parser::RemoveCommentsInScipts(std::string& psource)
{
	boost::regex pattern("<script(.*?)<\\/script>", boost::regex_constants::perl|boost::regex::icase);
	std::string xcontent = psource;

	boost::sregex_token_iterator it(xcontent.begin(), xcontent.end(), pattern, 1);
	boost::sregex_token_iterator end;
	
	std::string x;
	std::string mt;
	std::string orig;

	for(; it != end; ++it)
	{
		x = it->str();
		
		if (utils::Index(x.c_str(), "//") > -1)
		{		
			RemoveSlashComments(x);
			
			mt = "<script";
			mt.append(x);
			mt.append("</script>");

			orig = "<script";
			orig.append(it->str());
			orig.append("</script>");

			boost::replace_first(psource, orig, mt);				
		}
	}
}

inline void cgo::parser::RefactorWrite(std::string& psource)
{	
	std::string xcontent = psource;

	char* printcomds[] = JS_PRINT_COMMANDS;
	
	std::string re;
	std::string px;
	std::string match;
	boost::regex pattern;
	boost::smatch result;

	for (int i = 0; i < ARRLEN(printcomds); i++)
	{
		re = printcomds[i];
		re.append("\\s*(.*?)\\s*;");
		pattern.assign(re, boost::regex_constants::perl);
		px = JS_BUFFER;
		px.append(".push(");
	
		result;
		match;
  	
		if (boost::regex_search(xcontent, result, pattern)) 
		{
			match = result[1];
			boost::replace_all(match, "\\n", "\\\\n");
		}

		if (match.length() > 0)
		{
			if (utils::Index(match.c_str(), "(") == 0)
				match = match.substr(1,match.find_last_of(")")-1);
		
			px.append(match);
			px.append(");");
			px.append(JS_BUFFER_PUSH_END_TAG_REG);
		
			boost::replace_all(px, "\\\"", "\\\\\"");

			psource = boost::regex_replace(psource, pattern, px);
		}
	}
}

inline void cgo::parser::FixNewLineSlashes(std::string& psource)
{
	boost::replace_all(psource, "\\\n", "\\\\\n");
}

inline void cgo::parser::FixQuotes(std::string& psource)
{
	boost::regex pattern("\\\\\\\"", boost::regex_constants::perl);
	psource = boost::regex_replace(psource, pattern, "\\\\\"");
}

bool cgo::parser::CheckDuplicateIDs(std::string& psource, std::string& errstr)
{	
	boost::regex pattern("\\<[A-Za-z]+[.]*\\sid\\=[\"'](.*?)[\"']", boost::regex_constants::perl|boost::regex_constants::icase);
	
	boost::sregex_token_iterator it(psource.begin(), psource.end(), pattern, 1);
	boost::sregex_token_iterator end;
	
	std::string x;

	for(; it != end; ++it)
	{
		x = it->str();
		
		if (UNQ_IDS[x] == 1)
		{
			errstr = x;
			return true;
		 }
			UNQ_IDS[x] = 1;    
	}
	
	return false;
}
  
inline void cgo::parser::PreserveSpacesReverse(std::string& psource)
{	
	boost::replace_all(psource, "\\n", "\n");
}

inline bool cgo::parser::IrisScriptParse(std::string& psource)
{	
	if (utils::Index(psource.c_str(), "include(") > -1)
		if (!ParseInclude(psource))
			return false;
	
	if (utils::Index(psource.c_str(), "<script") > -1)
		RemoveCommentsInScipts(psource);
	
	char* printcomds[] = JS_PRINT_COMMANDS;

	for (int i =0; i < ARRLEN(printcomds); i++)
	{
		if (utils::Index(psource.c_str(), printcomds[i]) > -1)
		{
			IrisScriptLines(psource);
			break;
		}
	}

	RemoveSlashComments(psource);
	FixNewLineSlashes(psource);
	
	return true;
}

inline std::string cgo::parser::Include(const std::string& filenm, const std::string& path, std::string& errstr) 
{
   
   if (UNQ_INCLUDES[filenm] == 1)
	{
		errstr = "Duplicate Include: ";
		return "";
	}

	std::string fpath = path;
	fpath.append(filenm);
    
	FILE* file = fopen(fpath.c_str(), "rb");
  
	if (!file)
	{
		errstr = "Include Library not found: ";
		return "";
	}

	fseek(file, 0, SEEK_END);
	int size = ftell(file);
	rewind(file);

	char* chars = new char[size + 1];
	chars[size] = '\0';
	
	for(int i = 0; i < size;) 
	{
	int read = static_cast<int>(fread(&chars[i], 1, size - i, file));
	i += read;
	}
	fclose(file);
  
	UNQ_INCLUDES[filenm] = 1;
	
	std::string ret(chars);

	int pos = utils::Index(ret.c_str(), JS_INCLUDE_EXPORT_OPEN_TAG);
	int pos2 = utils::Index(ret.c_str(), JS_INCLUDE_EXPORT_CLOSE_TAG);
	
	if (pos < 0 || pos2 < 0)
	{
		errstr = "Invalid library format: ";
		return "";
	}
	
	pos += STRLEN(JS_INCLUDE_EXPORT_OPEN_TAG);
	ret = ret.substr(pos, pos2 - pos);
	
	RemoveComments(ret);
	RemoveSlashComments(ret);

	return ret; 
}

inline bool cgo::parser::ParseInclude(std::string& psource)
{ 
	std::string xcontent = psource;

	int xmatch = 1;
	std::string re; 
		
	if (ACTIVE_DEBUG)
	{
		re.append(JS_DEBUG_LINE_REGEX_OPTIONAL);
		xmatch = 2;
	}
	
	re.append("[\\s\\n]include\\s*\\(\\s*\\\"(.*?)\\\"\\s*\\)");
	boost::regex pattern(re, boost::regex_constants::perl);

	boost::match_results<std::string::const_iterator> what;
	std::string::const_iterator start = xcontent.begin();
	std::string::const_iterator end = xcontent.end();
	
	std::string errstr;
	std::string x;
	std::string xf;

	while(boost::regex_search(start, end, what, pattern)) 
	{
		x = what[xmatch];
		xf = x;
		xf.append(JS_LIB_EXTENSION);
		
		std::string xinc = Include(xf, Settings.LIB_PATH, errstr);

		if (xinc.empty())
		{
			if (Settings.USR_LIB_PATH.length() > 0)
				xinc = Include(xf, Settings.USR_LIB_PATH, errstr);

			if (xinc.empty())
			{
				std::string error_msg = errstr;

				if (Settings.IS_WEB)
					error_msg.insert(0, "<span style=\"color:red\">");
				
				error_msg.append(x);
				
				if (Settings.IS_WEB)
					error_msg.append("</span><BR>");
				
				error_msg.append("\nLibrary Filename: ");
				error_msg.append(xf);
				
				if (Settings.IS_WEB)	
					error_msg.append("<BR>");

				error_msg.append("\nPath(s): ");
				error_msg.append(Settings.LIB_PATH);
				
				if (Settings.USR_LIB_PATH.length() > 0)
				{
					error_msg.append(";");
					error_msg.append(Settings.USR_LIB_PATH);
				}

				throw GOLocalException(error_msg.c_str());

				return false;
			}
		}
		
		xf = "include(\"";
		xf.append(x);
		xf.append("\")");

		if (ACTIVE_DEBUG)
			xinc = debug::PrepareLinesIncludes(xinc, what[1]);
		
		boost::replace_first(psource, xf, xinc);
		
		start = what[0].second;
	}
	
	return true;
}


